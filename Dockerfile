FROM node:alpine

# Maintainer
MAINTAINER Aksenchyk V. <aksenchyk.v@gmail.com>

# Define app directory
WORKDIR /usr/src/app

# Create app directory
RUN mkdir -p /usr/src/app

# Copy app sources
COPY . /usr/src/app

# Install dependencies and build client

# Install dependencies and build client
RUN npm install -g -s --no-progress yarn && \
    yarn && \
    yarn cache clean && \
    npm install -g @angular/cli && \
    npm run build

# Make server and client available
EXPOSE 5601

CMD [ "npm", "start" ]
