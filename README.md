
## 200loc dashboard (Angular)

## Basic Usage
```bash
# clone the repo
$ git clone https://aksenchyk@bitbucket.org/aksenchyk/200loc-ui.git
$ cd oneopp-dashboard

# install 
$ yarn

# build for development/production
$ yarn build 

$ yarn dev 
# proxy server is configured for the gateway running on localhost

```
