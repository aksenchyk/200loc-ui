import { OneoppDashboard2Page } from './app.po';

describe('oneopp-dashboard2 App', () => {
  let page: OneoppDashboard2Page;

  beforeEach(() => {
    page = new OneoppDashboard2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
