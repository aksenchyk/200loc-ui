import {
    Component, Directive, Input, Output, OnInit,
    TemplateRef, ViewContainerRef, trigger, style, state, transition, ViewChild, keyframes, AnimationTransitionEvent,
    ContentChild, animate, OnDestroy, ViewChildren, QueryList, ChangeDetectorRef
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EventEmitter, Renderer, ElementRef } from '@angular/core';
import { ApiConfig, ServiceApi, ApiConfigApi, StatsActions, StatsService } from '../../core';
import { Observable } from 'rxjs';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { Store } from "@ngrx/store";
import { AppState, getStats, getHealth } from '../../core/reducers';

@Component({
    selector: 'entries-details',
    styleUrls: [
        './entries-list-details.component.scss'
    ],
    templateUrl: './entries-list-details.component.tmpl.html',
    animations: [
        trigger('detailsCollapse', [
            state('expanded', style({
                height: '*'
            })),
            state('collapsed', style({
                height: '0px'
            })),
            transition('collapsed => expanded', [
                animate('0.2s ease-out', style({ height: '*' }))
            ]),
            transition('expanded => collapsed', [
                animate('0.2s ease-in', style({ height: '0px' }))
            ])
        ]),
        trigger('headerCollapse', [
            state('expanded', style({
                background: '#fafafa'
            })),
            state('collapsed', style({
                background: '#fff'
            })),
            transition('expanded => collapsed', animate('250ms linear')),
            transition('collapsed => expanded', animate('250ms linear'))
        ])
    ]
})
export class EntriesDetailsComponent implements OnDestroy {
    @ViewChild('panel') container: ElementRef;
    @ViewChild('detailsContainer') detailsContainer: ElementRef;
    @ViewChild('content') content: TemplateRef<any>;
    @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;


    private _expand: boolean = false;
    private _disabled: boolean = false;
    state: string = 'collapsed';
    dialogOpened: boolean = false;
    showStatuses: boolean = false;
    loading: boolean = false;
    status: any = { error: false, message: 'N/A' };
    intervalHandler: any;
    average: any = 0;

    constructor(private _renderer: Renderer,
        private _elementRef: ElementRef,
        private _router: Router,
        private modalService: NgbModal,
        private _route: ActivatedRoute,
        private _statsActions: StatsActions,
        private ref: ChangeDetectorRef,
        private _statsService: StatsService,
        private _store: Store<AppState>        
    ) {
    }

    ngAfterViewInit() {
        this._store
            .let(getStats())
            .subscribe((result) => {
                this.loadChartData = [{ data: result.load, label: 'Req/s' }];
                this.loadChartLabels.length = 0;
                let labels = this.produceLabels();
                this.loadChartLabels.push(...labels);

                let statusesLabels = [];
                let statusesData = [];

                Object.keys(result.statuses || {})
                    .forEach((status) => {
                        statusesData.push(+result.statuses[status]);
                        statusesLabels.push(status);
                    });
                this.statusesChartLabels.length = 0;
                if (statusesLabels.length > 0) {
                    this.statusesChartLabels.push(...statusesLabels);
                } else {
                    this.statusesChartLabels.push('n/a');
                }
                this.statusesChartData.length = 0;
                if (statusesData.length > 0) {
                    this.statusesChartData.push(...statusesData);
                } else {
                    this.statusesChartData.push(1);
                }
                this.statusesChartColors[0].backgroundColor.length = 0;
                this.statusesChartColors[0].backgroundColor.push(...this.produceStatusesColors(this.statusesChartLabels));
                let chrartsArr = this.charts.toArray();
                if (chrartsArr[1] && chrartsArr[1].chart) {
                    chrartsArr[1].chart.update();
                };
                this.average = result.average || 0;
            })
    }

    @Input() configO: ApiConfig;
    @Input() label: string;
    @Input() sublabel: string;


    set expand(value: boolean) {
        this._expand = value;
        this.state = value ? 'expanded' : 'collapsed';
    };
    get expand(): boolean {
        return this._expand;
    };

    @Input('disabled')
    set disabled(disabled: boolean) {
        if (disabled && this._expand) {
            this._expand = false;
            this._onCollapsed();
        }
        this._disabled = disabled;
    };
    get disabled(): boolean {
        return this._disabled;
    };

    @Output() expanded: EventEmitter<void> = new EventEmitter<void>();
    @Output() collapsed: EventEmitter<void> = new EventEmitter<void>();

    clickEvent(): void {
        this._setExpand(!this.expand);
    };
    ngOnDestroy() {
        this.__stopStatsTimer();
    }

    public produceLabels = (): number[] => {
        var coeff = 1000;
        let now = Date.now();
        let rounded = coeff * Math.round(now / coeff);
        let aggregation = [];
        for (let i = 59; i >= 0; i--) {
            aggregation.push(rounded - i * 1000);
        }
        return aggregation;
    };

    public produceStatusesColors = (data: Array<string> = []) => {
        let colors = data.map((d) => {
            let color = 'grey';
            switch (d) {
                case "404": {
                    color = "#FF9C00";
                    break;
                }
                case "401": {
                    color = "#ff8300";
                    break;
                }
                case "400": {
                    color = "#ff6c00";
                    break;
                }
                case "200": {
                    color = "#7BB31A";
                    break;
                }
                default: {
                    if (+d >= 200) {
                        color = 'green';
                    }
                    if (+d >= 400) {
                        color = '#EEDB00';
                    }
                    if (+d >= 500) {
                        color = '#CE0000';
                    }
                    break;
                }
            }
            return color;
        });
        return colors;
    }

    public loadChartLabels = this.produceLabels();

    statusesChartOptions: any = {
        scaleShowVerticalLines: false,
        maintainAspectRatio: false,
        responsive: true,
        fonts: {
            defaultFontFamily: 'Open Sans'
        },
        animation: {
            duration: 0, // general animation time
        },
    }

    public min = Date.now();

    public loadChartOptions: any = {
        scaleShowVerticalLines: false,
        scaleBeginAtZero: true,
        maintainAspectRatio: false,
        responsive: true,
        defaultFontFamily: 'Open Sans',
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    suggestedMin: 0,
                    callback: (value) => {
                        return value % 1 === 0 ? +value : "";
                    },
                    fontColor: "#555",
                    fontSize: 11,
                    // stepSize: 1,
                },
            }],
            xAxes: [{
                type: 'time',
                ticks: {
                    beginAtZero: true,
                    //  fontColor: "#555",
                    //  fontSize: 11,
                    // fontColor: "transparent",
                    display: false
                },
                unit: 'second',
                unitStepSize: 1,
                time: {
                    displayFormats: {
                        'second': 'hh:mm:ss'
                    },
                    tooltipFormat: 'hh:mm:ss',
                },
                steps: 61,
            }]
        },
        elements: {
            line: {
                borderWidth: 2
            },
            point: {
                radius: 1
            }
        },
        animation: {
            duration: 0,
        },
    };

    public lineChartColors: Array<any> = [{
        backgroundColor: '#DEE7EF',
        borderColor: "#9CAAC6",
        pointBackgroundColor: '#5A79A5',
        pointBorderColor: '#9CAAC6',
        pointHoverBackgroundColor: '#000063',
        pointHoverBorderColor: '#000063'
    }];
    public loadChartType: string = "line";
    public statusesChartType = 'doughnut';
    public loadChartLegend: boolean = true;

    public statusesChartData: Array<any> = [1];
    public statusesChartLabels: Array<any> = ['n/a'];
    public statusesChartColors: any[] = [{ backgroundColor: [...this.produceStatusesColors(['n/a'])] }];
    public loadChartData: any[] = [
        { data: [], label: 'Req/s' }
    ];
    __startStatsTimer(): void {
        this.intervalHandler = setInterval(() => {
            this._statsService
                .stats('stats', { id: this.configO.id })
                .subscribe((result) => {
                    this._store.dispatch(this._statsActions.setStats(result.stats || {}));
                }, (err) => {
                    console.log(err);
                })
        }, 1000);
    }

    __stopStatsTimer(): void {

        if (this.intervalHandler) {
            clearInterval(this.intervalHandler);
        }
    }

    /**
     * Toggle expand state of [TdExpansionPanelComponent]
     * retuns 'true' if successful, else 'false'.
     */
    toggle() {
        this._setExpand(!this.expand);
        if (this.expand && this.configO && this.configO.errors.length > 0) {
            return;
        }
        if (this.expand && this.configO) {
            this._store
                .let(getHealth())
                .map((config) => config.find(((c) => c.id == this.configO.id)))
                .subscribe((config: any) => {
                    this.status = config || { message: 'n/a' };
                }, (err) => {
                    this.status = {
                        message: err,
                        error: true
                    };
                });
        }
    }

    /**
     * Opens [TdExpansionPanelComponent]
     * retuns 'true' if successful, else 'false'.
     */
    open(): boolean {
        return this._setExpand(true);
    }

    /**
     * Closes [TdExpansionPanelComponent]
     * retuns 'true' if successful, else 'false'.
     */
    close(): boolean {
        if (this.dialogOpened) return;
        return this._setExpand(false);
    }

    /**
     * Method to change expand state internally and emit the [onExpanded] event if 'true' or [onCollapsed]
     * event if 'false'. (Blocked if [disabled] is 'true')
     */
    private _setExpand(newExpand: boolean): boolean {
        if (this._disabled) {
            this.expand = false;
        }
        if (this.expand !== newExpand) {
            this.expand = newExpand;
            if (newExpand) {

                this._onExpanded();
            } else {

                this._onCollapsed();
            }
            return true;
        }
        return false;
    };

    private _onExpanded(): void {
        if (this.configO && this.configO.active) {
            this.__startStatsTimer();
        }

        this.expanded.emit(undefined);
    };
    private _onCollapsed(): void {
        this.__stopStatsTimer();
        this.collapsed.emit(undefined);
    };


    openModal() {
        this.dialogOpened = true;
        const modalRef = this.modalService
            .open(this.content, { windowClass: 'statistic-modal' });

        modalRef.result.then((result) => {
            console.log('DIALOG CLOSING')
            this.dialogOpened = false;
        }, (reason) => {
            console.log('DIALOG CLOSING2')
            setTimeout(() => {
                this.dialogOpened = false;
            }, 100)

        });
    }
}