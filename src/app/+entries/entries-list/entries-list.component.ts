import { Component, OnInit, OnDestroy, trigger, state, transition, style, animate, ViewChild, ElementRef, NgZone, EventEmitter, TemplateRef } from '@angular/core';
import { LoaderComponent, LocPagerComponent } from '../../shared/components';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { ApiConfigApi, ApiConfig, ServiceApi, ServiceStatus, LoopBackAuth } from '../../core'
import { FormControl } from '@angular/forms';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap'
import { PagerComponent } from 'ngx-bootstrap';
import { saveAs } from "file-saver";
import { CoreConfig } from '../../core/core.config';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppState, getHealth } from '../../core/reducers';
import { Store } from '@ngrx/store';

interface IQuery {
    name?: string;
    page?: number;
}
    
@Component({
    selector: 'api-list',
    templateUrl: './entries-list.component.tmpl.html',
    styleUrls: ['./entries-list.scss'],
    animations: [
        trigger('initial', [
            state('start', style({
                visibility: 'hidden',
                opacity: 0
            })),
            state('complete', style({
                visibility: 'visible',
                opacity: 1
            })),
            transition('start => complete', [
                animate('300ms linear')
            ])
        ])
    ]
})
export class EntriesListComponent implements OnInit, OnDestroy {
    @ViewChild('searchInput') searchInput: ElementRef;
    @ViewChild(PagerComponent) pager: PagerComponent;
    @ViewChild(LocPagerComponent) locPager: LocPagerComponent;
    @ViewChild('importPreview') content: TemplateRef<any>;
    @ViewChild('uploadEl') uploadElRef: ElementRef;

    configs: Array<any> = [];
    loading: boolean = false;
    sidebarActive: boolean = false;
    serviceStatusArray: Array<ServiceStatus> = [];
    initialComplete: boolean = false;
    state: string = 'start';
    count: number;
    searchParams: IQuery = {};
    maxItems: number = 10;
    showMenu: boolean = false;
    searchQuery: string;
    searchControl: FormControl = new FormControl();
    public uploader: FileUploader;
    modalRef: NgbModalRef;
    importData: { routes: any[] } = { routes: [] };

    updateIntervalHandler: any;
    constructor(       
        private router: Router,
        private route: ActivatedRoute,
        private _serviceApi: ServiceApi,
        private _authService: LoopBackAuth,
        private _ngZone: NgZone,
        private modalService: NgbModal,
        private _apiConfigApi: ApiConfigApi,
        private _store: Store<AppState>

    ) {
        let header: any = { 'name': "Authorization", 'value': _authService.user ? _authService.user.accessToken : '' };
        this.uploader = new FileUploader({
            url: '/api/upload',
            method: 'post',
            headers: [header],
            autoUpload: true
        });
        this.uploader.onSuccessItem = (item: any, response: string, status: number) => {
            this.uploader.clearQueue();
            this.modalRef = this.modalService
                .open(this.content, { windowClass: 'import-modal' });
            this.modalRef.result.then((result) => {
                this.importData = { routes: [] };
            }, (reason) => {
                this.importData = { routes: [] };
            });
            try {
                this.importData = JSON.parse(response);
                this.importData.routes = this.importData.routes.map((r) => {
                    r.active = false;
                    return r;
                });              
            } catch (error) {
                console.error(error);
            }
        }
    }

    ngOnInit() {
        this.searchParams = {
            name: this.route.snapshot.queryParams['name'] || '',
            page: +this.route.snapshot.queryParams['page'] || 1
        }
        this.route.queryParams
            .do((qParams) => { this.loading = true })
            .map((qParams: any) => {
                let q: any = {
                    limit: this.maxItems,
                    skip: +qParams.page && +qParams.page > 0 ? this.maxItems * (+qParams.page - 1) : 0
                }
                if (qParams && qParams.name && qParams.name.trim) {
                    q.where = { name: `${qParams.name.trim()}` }
                }
                return q;
            })
            .switchMap((query: any) => this._doSearchQuery(query))
            .do(() => { this.initialComplete = true; this.state = 'complete' })
            .subscribe((result: any) => {
                this.configs = result.configs;
                this.count = result.count;
                this.loading = false;
                if (this.locPager) {
                    this.locPager.total = this.count;
                    this.locPager.navigateToPage(this.searchParams.page || 1, false);
                }
            }, (err) => {
                console.error(err);
                this.loading = false;
            });

        this.updateIntervalHandler = this._store
            .let(getHealth())
            .subscribe((state: Array<any>) => {
                this.serviceStatusArray = state;
                this.configs = this.configs.map(c => this.setStatus(c));
            }, (err) => { });
    }

    _doSearchQuery(query) {
        return Observable.zip(
            this._apiConfigApi.find(query),
            this._apiConfigApi.count(!!query.where ? query.where : {})
            , (configs: any, conuntResult: any) => {
                this.configs = configs.map(c => this.setStatus(c));
                return {
                    configs: configs,
                    count: conuntResult.count
                }
            });
    }

    ngAfterViewInit() {
        this.uploader.onAfterAddingFile = (item => {
            this.uploadElRef.nativeElement.value = ''
        });
        this.searchControl
            .valueChanges
            .distinctUntilChanged()
            .do(() => this.loading = true)
            .debounceTime(500)
            .map((value) => { return { name: value, page: 1 } })
            .subscribe((q: IQuery) => {
                this.doSearch(q);
            })

        if (this.locPager)
            this.locPager
                .pageChanged
                .distinctUntilChanged()
                .do(() => this.loading = true)
                .map((value) => { return { page: value.page } })
                .subscribe((q: IQuery) => {
                    this.doSearch(q);
                });
    }

    doSearch(q: IQuery) {
        const req = Object.assign(this.searchParams, q)
        console.log(req)
        this.router.navigate(['/entries'], { queryParams: req });
    }

    ngOnDestroy() {
        if (this.updateIntervalHandler) {
            clearInterval(this.updateIntervalHandler);
        }
    }

    onRemove(config) {
        this._apiConfigApi
            .deleteById(config.id)
            .subscribe(res => {
                let ind = this.configs.indexOf(config);
                this.configs.splice(ind, 1);
                this.__onSearch();
            }, (err) => {
                console.log(err);
            });
    }


    setStatus(config: ApiConfig) {
        const messages = new Array<ServiceStatus>();
        const st = this.serviceStatusArray.find(s => s.id == config.id);
        if (st) {
            messages.push(st);
            let type = "error";
            if ((st.message === 'disabled' || st.message === 'healthy')) {
                type = st.message;
            }
            return Object.assign(config, { type: type });
        } else {
            return Object.assign(config, { type: 'disabled' });
        }
    }

    editApi(config) {
        this.router.navigate(['./master'], { queryParams: { id: config.id }, relativeTo: this.route })
    }

    toggleActive(config, value) {
        this.loading = true;
        this._apiConfigApi.updateOrCreate(Object.assign({}, config, {
            active: value,
            loaded: null,
            status: null
        }))
            .subscribe((result) => {
                this.loading = false;
                this.__onSearch();
            }, (err) => {
                console.error(err);
            });
    }

    onDetails(config: ApiConfig) {

    }

    onAddClick() {
        this.router.navigate(['./master'], { relativeTo: this.route });
    }

    __onSearch() {
        this.loading = true;
        Observable.of(this.searchParams)
            .map((qParams: any) => {
                let q: any = {
                    limit: this.maxItems,
                    skip: +qParams.page && +qParams.page > 0 ? this.maxItems * (+qParams.page - 1) : 0
                }
                if (qParams && qParams.name && qParams.name.trim) {
                    q.where = { name: { regexp: `/^${qParams.name.trim()}/i` } }
                }
                return q;
            })
            .switchMap((q) => this._doSearchQuery(q))
            .subscribe((result: any) => {
                this.configs = result.configs;
                this.count = result.count;
                this.loading = false;

                if (this.locPager) {
                    this.locPager.total = this.count;
                    this.locPager.navigateToPage(this.searchParams.page || 1, false);
                }
            }, (err) => {
                console.error(err);
                this.loading = false;
            });
    }

    getConfigMethodsString(config: ApiConfig) {
        return (config.methods || []).join(', ');
    }

    getRulesCount(config) {
        return config.rules ? config.rules.length : '-';
    }
    getRulesPath(config: ApiConfig) {
        return config.rules && config.rules.length > 0 ? config.rules.map((r) => `${r.path}\n`) : 'n/a';
    }

    onClone(config) {
        this.loading = true;
        this._apiConfigApi.clone(config.id)
            .subscribe((cloned: any) => {
                this.loading = false;
                this.router.navigate(['./master'], { queryParams: { id: cloned._id }, relativeTo: this.route });
            }, (err) => {
                this.loading = false;
                console.error(err);
            });
    }
    onExportClick() {
        this.loading = true;
        this._apiConfigApi.export()
            .subscribe((result: Blob) => {
                this.loading = false;
                saveAs(result, 'loc.json');
            }, (err) => {
                this.loading = false;
                console.error(err);
            });
    }

    toggleRouteReadyToBeInserted(route) {
        route.active = !route.active;
    }

    isDisabledModalImportInsertButton() {
        return !(this.importData.routes.find((r) => r.active));
    }

    selectAll() {
        this.importData.routes.forEach((r) => {
            r.active = true;
        });
    }

    insertSelected(importData) {
        let routesToImport = (importData.routes || []).filter((r) => r.active);       
        this.loading = true;
        this._apiConfigApi.import({ routes: routesToImport })
            .subscribe((result) => {
                console.log(result);
                this.loading = false;
                if (this.modalRef) {
                    this.modalRef.close();
                    this.__onSearch();
                }
            }, (err) => {
                this.loading = false;
                console.error(err);
            });
    }

    showSideMenu(config) {
        this.sidebarActive = true;
    }
}
