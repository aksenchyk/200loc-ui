import { Component, OnInit, ViewChild, QueryList } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Config, ApiConfigApi, IPlugin, Proxy, Rule, Health, EntryOption } from "../../core";
import { UiTabs, UiPane } from '../shared';
import { Observable, Subscription } from "rxjs";
import { Store } from '@ngrx/store';
import { AppState, getConfigState, getMasterState, getValidationState } from '../../core/reducers';
import { MasterActions, ConfigActions } from '../../core/actions';

@Component({
  selector: "api-master",
  templateUrl: './entries-wizard-base.component.tmpl.html'
})
export class EntriesWizardBaseComponent {

  @ViewChild(UiTabs) tab: UiTabs;
  id: string;
  loading: boolean = true;
  submitted: boolean = false;
  queryRouteSub: Subscription;
  masterStateSub_n: Subscription;
  validationSub_n: Subscription;
  validation: any = {};
  config: Config = {};

  constructor(
    private router: Router,
    private _activatedRoute: ActivatedRoute,
    private _configActions: ConfigActions,
    private _masterActions: MasterActions,
    private apiConfigApi: ApiConfigApi,
    public store: Store<AppState>) { }

  ngOnInit() {

    this.validationSub_n = this.store
      .let(getValidationState())
      .subscribe((validation) => {
        this.validation = validation;
      });

    this.masterStateSub_n = this.store
      .let(getMasterState())
      .subscribe((config) => {
        this.config = config;
      });
    this.queryRouteSub = this._activatedRoute
      .params
      .subscribe((params) => {
        Promise.resolve().then(() => {
          let tab = params["t"];
          if (tab === 'routes') {
            this.tab.goTo('plugins');
          } else {
            this.tab.goTo('general');
          }
        });
      });
  }

  ngAfterViewInit() {
    this.queryRouteSub = this._activatedRoute
      .queryParams
      .flatMap((params): any => {
        this.id = params["id"];
        return this.id
          ? this.apiConfigApi.findById(this.id)
          : Observable.of(new Config());
      })
      .subscribe((config) => {
        this.store.dispatch(this._configActions.setConfig(config));
        this.store.dispatch(this._masterActions.setConfig(config));
      });
  }

  ngOnDestroy() {
    if (this.queryRouteSub) this.queryRouteSub.unsubscribe();
    if (this.masterStateSub_n) this.masterStateSub_n.unsubscribe();
    if (this.validationSub_n) this.validationSub_n.unsubscribe()
  }

  onDone() {
    this.submitted = true;

    for (let KEY in this.validation) {
      if (!this.validation[KEY]) {
        this.tab.goTo(KEY);
        return;
      }
    }
    let plugins: Array<IPlugin> = [...this.config.plugins];

    plugins = plugins.map((plugin) => {
      return {
        displayName: plugin.displayName,
        description: plugin.description || '',
        name: plugin.name,
        order: plugin.order,
        settings: plugin.settings,
        dependencies: plugin.dependencies
      }
    });

    let proxy: Proxy = this.config.proxy;
    let rules: Rule[] = this.config.rules;
    let health: Health = this.config.health;
    let conditions: EntryOption[] = this.config.conditions;

    let apiConfig = Object.assign(this.config, { plugins: plugins, proxy: proxy, rules: rules, health: health, conditions: conditions })
    console.log("Saving configuration", apiConfig);
    this.apiConfigApi
      .updateOrCreate(apiConfig)
      .subscribe((result) => {
        this.router.navigate(['/']);
      }, (err) => {
        // notify
      });
  }
}