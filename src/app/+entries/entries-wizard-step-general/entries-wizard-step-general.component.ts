import { Component, OnInit, AfterViewInit, Output, Input, EventEmitter, OnDestroy, Host, Optional } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Config } from '../../core/models';
import { AppController } from '../../shared/services';
import { LoaderComponent } from '../../shared/components';
import { Observable, Subscription, BehaviorSubject, ReplaySubject } from 'rxjs';

import { Store } from '@ngrx/store';
import { AppState, getConfigState } from '../../core/reducers';
import { MasterActions, ValidationActions } from '../../core/actions';

@Component({
    selector: 'step-general',
    templateUrl: "./entries-wizard-step-general.component.tmpl.html",
    styleUrls: ['./stepGeneral.scss']
})
export class EntriesWizardStepGeneral {
    @Output()
    next: EventEmitter<any> = new EventEmitter();
    @Output()
    save: EventEmitter<any> = new EventEmitter();
    @Output()
    validation: BehaviorSubject<boolean> = new BehaviorSubject(true);
    form: FormGroup;

    apiConfig: any = {
        methods: []
    };

    loading: boolean = false;
    configStateSub_n: Subscription;
    @Input()
    submitted: boolean = false;
    valid = false;

    constructor(
        private _store: Store<AppState>,
        private _masterActions: MasterActions,
        private _validationActions: ValidationActions,
        private _fb: FormBuilder) {
        this.form = _fb.group({
            name: ["", Validators.required],
            entry: ["", Validators.required],
            description: [""]
        });
    }
    ngOnInit() {
        this.validation.next(true);
        this.loading = true;
        this.form
            .valueChanges
            .distinctUntilChanged()
            .subscribe((value) => {
                for (let KEY in value) {
                    if (value[KEY] == undefined) return;
                }
                this._store.dispatch(this._masterActions.setGeneralInfoData(value));
            });

        this.form
            .statusChanges
            .subscribe((value) => {
                Promise.resolve().then(() => {
                    console.log(this.form.valid)
                    if (this.valid !== this.form.valid) {
                        this.valid = this.form.valid
                        this._store.dispatch(this._validationActions.setValidity({ general: this.valid }));
                        this.validation.next(this.valid);
                    }
                });
            });

        this.configStateSub_n = this._store
            .let(getConfigState())
            .subscribe((config) => {
                if (!config) return;
                Promise.resolve()
                    .then(() => {
                        this.loading = false;
                        this.apiConfig = config;
                    });
            });
    }

    ngOnDestroy() {
        if (this.configStateSub_n) {
            this.configStateSub_n.unsubscribe();
        }
    }

    onSubmit() {
        this.submitted = true;
        this._store.dispatch(this._validationActions.setValidity({ general: this.form.valid }));
        this.validation.next(this.form.valid);
        if (this.form.valid) {
            this.next.emit('plugins');
        }
    }

    onSave() {
        this.submitted = true;
        this._store.dispatch(this._validationActions.setValidity({ general: this.form.valid }));
        this.validation.next(this.form.valid);
        if (this.form.valid) {
            this.save.emit('Done');
        }
    }
}
