import { Component, OnInit, AfterViewInit, Output, Input, EventEmitter, OnDestroy, Host, Optional } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Config } from '../../core/models';
import { AppController } from '../../shared/services';
import { LoaderComponent } from '../../shared/components';
import { Observable, Subscription } from 'rxjs';

import { Store } from '@ngrx/store';
import { AppState, getConfigState } from '../../core/reducers';
import { MasterActions, ValidationActions, HealthCheck, Health, ApiConfigApi } from '../../core';

@Component({
    selector: 'step-healthcheck',
    templateUrl: "./entries-wizard-step-healthcheck.component.tmpl.html",
    styleUrls: ['./stephealthcheck.scss']
})
export class EntriesWizardStepHealthcheck implements AfterViewInit {
    @Output()
    save: EventEmitter<any> = new EventEmitter();
    @Output()
    next: EventEmitter<any> = new EventEmitter();
    @Output()
    validation: EventEmitter<any> = new EventEmitter();
    healthForm: FormGroup;

    healthcheck: any = [];

    loading: boolean = false;
    configStateSub_n: Subscription;
    @Input()
    submitted: boolean = false;
    config: Config;
    active: boolean;
    interval: number;
    healthcheckresult: any;

    checking: boolean = false;
    constructor(
        private _store: Store<AppState>,
        private _masterActions: MasterActions,
        private _validationActions: ValidationActions,
        private _configService: ApiConfigApi,
        private _fb: FormBuilder) {
        this.healthForm = this._fb.group({
            interval: [],
            active: []
        });
    }

    ngOnInit() {
        this.loading = true;
        this.healthForm.valueChanges
            .subscribe((value) => {
                this._apply();
            })
    }


    ngAfterViewInit() {
        this.configStateSub_n = this._store
            .let(getConfigState())
            .subscribe((config) => {
                if (!config) return;
                Promise.resolve().then(() => {
                    this.loading = false;
                    this.config = config;
                    this.healthcheck = config.health ? config.health.healthCheck || [] : [];
                    this.active = config.health ? config.health.active : false;
                    this.interval = config.health ? config.health.interval : 5000;
                });
            });
    }
    ngOnDestroy() {
        if (this.configStateSub_n) {
            this.configStateSub_n.unsubscribe();
        }
    }

    onRemoveRule(rule) {
        let ind = this.healthcheck.indexOf(rule);
        this.healthcheck.splice(ind, 1);
        this._apply();
    }

    onSave() {
        this.submitted = true;
        this.save.emit();
    }
    onSubmit() {
        this.submitted = true;
        this.next.emit('preview');
    }

    onChange() {
        this._apply();
    }

    _apply() {
        this._store.dispatch(this._masterActions.setHealthCheckData(Object.assign({}, { healthCheck: [...this.healthcheck], active: this.active, interval: this.interval })));
        this._store.dispatch(this._validationActions.setValidity({ health: true }));
    }

    onTryNow() {
        this.checking = true;
        this._configService.healthcheck({ healthCheck: this.healthcheck })
            .subscribe((result) => {
                this.checking = false;
                this.healthcheckresult = result;
            }, (err) => {
                this.checking = false;
                console.error(err);
            });
    }

    onAddNew() {
        this.healthcheck.push(new HealthCheck({ target: this.config.proxy.target || 'localhost', withPath: '/healthcheck', method: 'HEAD', interval: 2000 }))
    }
}

