import {
    Component, OnInit, AfterViewInit, Output,
    Input, EventEmitter, OnDestroy, Host, Optional,
    trigger, state, style, animate, transition
} from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Config, HealthCheck } from '../../core/models';

import { Observable, Subscription } from 'rxjs';

import { Store } from '@ngrx/store';
import { AppState, getConfigState } from '../../core/reducers';
import { MasterActions, ValidationActions } from '../../core/actions';

@Component({
    selector: 'healthcheck-rule',
    templateUrl: "./healthcheck-rule.component.tmpl.html",
    styleUrls: ['./healthcheck-rule.scss'],
    animations: [
        trigger('detailsCollapse', [
            state('expanded', style({
                height: '*',
                visibility: 'visible',
                opacity: '1'
            })),
            state('collapsed', style({
                height: '0px',
                visibility: 'hidden',
                opacity: '0'
            })),
            transition('expanded => collapsed', animate('250ms cubic-bezier(0.4,0.0,0.2,1)')),
            transition('collapsed => expanded', animate('250ms cubic-bezier(0.4,0.0,0.2,1)'))
        ]),
        trigger('headerCollapse', [
            state('expanded', style({
                background: '#fafafa',
                borderColor: 'red'
            })),
            state('collapsed', style({
                background: '#fff'
            })),
            transition('* => *', animate('250ms linear'))
        ]),
        trigger('iconState', [
            state('expanded', style({ transform: 'rotate(0deg)' })),
            state('collapsed', style({ transform: 'rotate(180deg)' })),
            transition('expanded => collapsed', animate('.4s ease-in')),
            transition('collapsed => expanded', animate('.4s ease-out')),
        ])
    ]
})
export class HealthcheckRule {
    @Output()
    change: EventEmitter<any> = new EventEmitter();
    @Output()
    remove: EventEmitter<any> = new EventEmitter();

    state: string = 'collapsed';

    healthRuleForm: FormGroup;
    private _healthCheckRule;
    @Input()
    public get healthCheckRule(): HealthCheck {
        return this._healthCheckRule;
    }

    public set healthCheckRule(v: HealthCheck) {
        if (v)
            this._healthCheckRule = v;
    }


    constructor(
        private _store: Store<AppState>,
        private _masterActions: MasterActions,
        private _validationActions: ValidationActions,
        private _fb: FormBuilder) {
        this.healthRuleForm = this._fb.group({
            target: [],
            withPath: [],
            method: [],
            timeout: []
        });

    }

    ngOnInit() {
        this.healthRuleForm
            .valueChanges
            .distinctUntilChanged()
            .subscribe((value) => {
                for (let KEY in value) {
                    if (value[KEY] == undefined) return;
                }
                if(this.healthRuleForm.valid){
                    this.change.emit(value);
                }
            });
    }  

    close() {

    }

    toggle() {
        if (this.state == 'collapsed') {
            this.state = 'expanded';
            return;
        }
        if (this.state == 'expanded') {
            this.state = 'collapsed';
        }
    }
}

