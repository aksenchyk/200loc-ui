import { EntriesWizardStepHealthcheck } from './entries-wizard-step-healthcheck.component';
import { HealthcheckRule } from './healthcheck-rule.component';
export * from './entries-wizard-step-healthcheck.component';

export const HEALTHCHECK_COMPONENTS = [
    HealthcheckRule, EntriesWizardStepHealthcheck
]