import {
    Component, OnInit, AfterViewInit, Output,
    Input, EventEmitter, OnDestroy, Host, Optional,
    trigger, state, style, animate, transition
} from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Config, EntryOption } from '../../core/models';

import { Observable, Subscription } from 'rxjs';

import { Store } from '@ngrx/store';
import { AppState, getConfigState } from '../../core/reducers';
import { MasterActions, ValidationActions } from '../../core/actions';

@Component({
    selector: 'entry-condition',
    templateUrl: "./entries-option.component.tmpl.html",
    styleUrls: ['./entries-option.scss'],
    animations: [
        trigger('detailsCollapse', [
            state('expanded', style({
                height: '*',
                visibility: 'visible',
                opacity: '1'
            })),
            state('collapsed', style({
                height: '0px',
                visibility: 'hidden',
                opacity: '0'
            })),
            transition('expanded => collapsed', animate('250ms cubic-bezier(0.4,0.0,0.2,1)')),
            transition('collapsed => expanded', animate('250ms cubic-bezier(0.4,0.0,0.2,1)'))
        ]),
        trigger('headerCollapse', [
            state('expanded', style({
                background: '#fafafa',
                borderColor: 'red'
            })),
            state('collapsed', style({
                background: '#fff'
            })),
            transition('* => *', animate('250ms linear'))
        ]),
        trigger('iconState', [
            state('expanded', style({ transform: 'rotate(0deg)' })),
            state('collapsed', style({ transform: 'rotate(180deg)' })),
            transition('expanded => collapsed', animate('.4s ease-in')),
            transition('collapsed => expanded', animate('.4s ease-out')),
        ])
    ]
})
export class EntryOptionComponent {
    @Output()
    change: EventEmitter<any> = new EventEmitter();
    @Output()
    remove: EventEmitter<any> = new EventEmitter();

    state: string = 'collapsed';

    optionForm: FormGroup;
    private _condition;
    @Input()
    public get condition(): EntryOption {
        return this._condition;
    }

    public set condition(v: EntryOption) {
        if (v)
            this._condition = v;
    }


    constructor(
        private _store: Store<AppState>,
        private _masterActions: MasterActions,
        private _validationActions: ValidationActions,
        private _fb: FormBuilder) {
        this.optionForm = this._fb.group({
            source: [],
            key: [],
            value: [],
            operation: [],
            name: []
        });

    }

    ngOnInit() {
        this.optionForm
            .valueChanges
            .distinctUntilChanged()
            .subscribe((value) => {
                for (let KEY in value) {
                    if (value[KEY] == undefined) return;
                }
                if (this.optionForm.valid) {
                    this.change.emit(value);
                }
            });
    }

    close() {

    }

    toggle() {
        if (this.state == 'collapsed') {
            this.state = 'expanded';
            return;
        }
        if (this.state == 'expanded') {
            this.state = 'collapsed';
        }
    }

    __getTitle() {
        if (this.condition && this.condition.source && this.condition.key && this.condition.value) {
            let base = `${this.condition.source} ${this.condition.key} `;
            return this.condition.operation == "eq" ? base + `equal ` + this.condition.value : base + "not equal " + this.condition.value;
        }else{
            return "Set condition"
        }
    }
}

