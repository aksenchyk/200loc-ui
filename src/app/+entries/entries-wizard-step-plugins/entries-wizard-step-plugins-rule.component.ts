import {
    Component, Directive, Input, Output, EventEmitter,
    TemplateRef, ViewContainerRef, trigger, style, state, transition, ViewChild, keyframes, AnimationTransitionEvent,
    ContentChild, animate
} from '@angular/core';

import { Config, Plugin, Rule, Proxy, EntryOption } from '../../core/models';
import { PluginSet } from '../shared';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'plugins-set-rule',
    templateUrl: "./entries-wizard-step-plugins-rule.tmpl.html",
    styleUrls: ['./set-plugins-rule.scss', './plugins-rule-editor.scss'],
    animations: [

        trigger('iconState', [
            state('expanded', style({ transform: 'rotate(0deg)' })),
            state('collapsed', style({ transform: 'rotate(45deg)' })),
            transition('expanded => collapsed', animate('.15s ease-in')),
            transition('collapsed => expanded', animate('.15s ease-out')),//close-round
        ]),
        trigger('buttonState', [
            state('expanded', style({ boxShadow: 'rgba(0, 0, 0, 0.05) 0px 0px 15px 1px inset' })),
            state('collapsed', style({ boxShadow: 'none' })),
            transition('expanded => collapsed', animate('.15s ease-out')),
            transition('collapsed => expanded', animate('.15s ease-in')),//close-round
        ]),
        trigger('detailsCollapse', [
            state('expanded', style({
                height: '*',
                opacity: 1
            })),
            state('collapsed', style({
                height: '0px',
                opacity: 0
            })),
            // transition('collapsed => expanded', [
            //     animate('0.2s ease-out', style({ height: '*' }))
            // ]),
            // transition('expanded => collapsed', [
            //     animate('0.2s ease-in', style({ height: '0px' }))
            // ]),
            // transition('void => *', animate('.0s ease-out')),
        ]),
    ]
})
export class StepPluginsRule {
    @Input()
    activePlugins: any;

    private _allPlugins = [];
    @Input()
    get allPlugins() {
        return this._allPlugins;
    };
    @Input()
    basicPath: string;

    set allPlugins(value) {
        if (value && Array.isArray(value)) {
            this._allPlugins = [...value];
        }
    }

    @Output()
    onSave: EventEmitter<any> = new EventEmitter();

    state: string = 'collapsed';
    tab: string = 'proxy';

    _preparedForAdding: any;
    _modalRef: NgbModalRef;

    @Output()
    selected: EventEmitter<Plugin> = new EventEmitter<Plugin>();

    @Output()
    deleted: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    added: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    activated: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(PluginSet) pluginsSet: PluginSet;

    constructor() { }

    private _rule: Rule;

    @Input()
    set rule(value) {
        if (value) {
            this._rule = Object.assign({}, value);
            if (this._rule.plugins && this._rule.plugins.length > 0) {
                this.pipePlugins = this._rule.plugins.map((p) => new Plugin(p));
                this.setPluginActive(this.pipePlugins[0]);
            }
            if (!this._rule.proxy) {
                this._rule.proxy = new Proxy({
                    target: 'http://localhost:80',
                    active: false,
                    prependPath: true,
                    secure: false,
                    ignorePath: false,
                    changeOrigin: false,
                    toProxy: false,
                    xfwd: false,
                    auth: '',
                    ws: false,
                    proxyTimeout: 1000
                });
            }
            if (!this._rule.methods) {
                this._rule.methods = [];
            }
            if (!this._rule.plugins) {
                this._rule.plugins = [];
            }
            if (!this._rule.conditions) {
                this._rule.conditions = [];
            }
        }
    }
    get rule() {
        return this._rule;
    };

    toggle() {
        if (this.state == 'collapsed') {
            this.state = 'expanded';
            return;
        }
        if (this.state == 'expanded') {
            this.state = 'collapsed';
        }
    }

    savePath() { }

    pipePlugins: Array<any> = [];
    activePlugin: Plugin;
    editable: Plugin;

    transferDataSuccess(event) {
        let plugin = new Plugin(Object.assign({}, event.dragData, { settings: {}, dependencies: {} }));

        this.pipePlugins = [...this.pipePlugins, plugin];
        this.setPluginActive(plugin);
    }

    setPluginActive(plugin) {
        this.editable = Object.assign({}, plugin);
        this.activePlugin = plugin;

    }

    deletePlugin(plugin) {
        let ind = this.pipePlugins.indexOf(plugin);
        this.pipePlugins.splice(ind, 1);
        if (this.pipePlugins.length > 0) {
            this.setPluginActive(this.pipePlugins[0]);
        } else {
            this.activePlugin = undefined;
            this.editable = undefined;
        }
    }

    pluginValueChanged(value) {
        this.activePlugin.settings = { ...value.settings };
        this.activePlugin.dependencies = { ...value.dependencies };
        this.rule.plugins = this.pipePlugins;
    }

    pluginValidationChanged(isValid) {
        //    this.activePlugin.valid = isValid;
    }

    // save() {
    //     this.rule.plugins = this.pipePlugins;
    //     this.onSave.emit(this.rule);
    // }

    setFlowView() {
        this.tab = 'flow';
    }

    setProxyView() {
        this.tab = 'proxy';
    }

    setConditionsView() {
        this.tab = 'conditions';
    }

    setPolicyView() {
        this.tab = 'policy';
    }

    onAddNewConditions() {
        this._rule.conditions.push(new EntryOption())
    }

    onDeleteCondition(condition) {
        let ind = this._rule.conditions.indexOf(condition);
        if (ind != -1)
            this._rule.conditions.splice(ind, 1);
    }
    onConditionsChanged(event) {

    }

    onPolicyChanged(policy) {
        this._rule.policy = policy;
        console.log(policy);
    }
}