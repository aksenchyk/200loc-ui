import {
    Component, OnInit,
    AfterViewInit, Output, Input,
    EventEmitter, OnDestroy, Host,
    ViewChildren, QueryList, TemplateRef, ViewChild, ViewContainerRef
} from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Config, Plugin, Rule, Proxy, EntryOption } from '../../core/models';
import { AppState, getConfigState, getPlugins, getMasterConfigPlugins, getMasterState } from '../../core/reducers';
import { MasterActions, ValidationActions } from '../../core/actions';
import { ApiConfigApi } from '../../core';

import { Observable, Subject, Subscription, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';

import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'step-plugins',
    templateUrl: "./entries-wizard-step-plugins.component.tmpl.html",
    styleUrls: ['./stepPlugins.scss']
})
export class EntriesWizardStepPlugins {
    _apiConfig;
    @Output()
    next: EventEmitter<any> = new EventEmitter();
    @Output()
    save: EventEmitter<any> = new EventEmitter();
    //@Output()
    //validation: EventEmitter<any> = new EventEmitter();
    @Output()
    validation: BehaviorSubject<boolean> = new BehaviorSubject(true);

    activePlugin: Plugin;
    activeRule: any;
    proxy: any = {};

    plugins: Array<Plugin> = [];

    rules: Array<any> = [];
    conditions: Array<EntryOption> = [];
    policy: string;

    loading: boolean;
    @Input()
    submitted: boolean = false;


    configStateSub_n: Subscription;
    pluginsSub_n: Subscription;
    proxySub_n: Subscription;


    @ViewChild('pluginsModal') editPluginsContent: TemplateRef<any>;
    @ViewChild('proxyModal') editProxyContent: TemplateRef<any>;

    basicPath: string;
    _modalRef: NgbModalRef;
    underEdit: any = {};
    showError: boolean = false;
    basicRule: Rule = {
        path: '*',
        plugins: [],
        conditions: []
    }

    constructor(
        private _masterActions: MasterActions,
        private modalService: NgbModal,
        private _validationActions: ValidationActions,
        private _apiConfigApi: ApiConfigApi,
        private _store: Store<AppState>) {
    }

    ngOnInit() {
        this._store.let(getMasterState())
            .subscribe((config: Config) => {
                Promise.resolve()
                    .then(() => {
                        this.basicPath = config.entry;
                    })
            });

        this.loading = true;
        this.configStateSub_n =
            this._store.let(getPlugins())
                .do((plugins) => this.plugins = plugins || [])
                .flatMap(() => this._store.let(getConfigState()))
                .subscribe((config) => {
                    if (!config) return;

                    Promise.resolve()
                        .then(() => {
                            this.proxy = config.proxy || this.proxy;

                            // Setup rules
                            const rules: any = [...config.rules];
                            (rules.plugins || [])
                                .map((plugin) => {
                                    const p = this.plugins.find(plug => plug.name === plugin.name);
                                    var plCp = Object.assign({}, p);
                                    return new Plugin(Object.assign({}, plCp, { settings: plugin.settings, dependencies: plugin.dependencies }));
                                });

                            // Setup plugins for main flow                    
                            const plugins = [...config.plugins];
                            this.basicRule.plugins = [];
                            this.basicRule.conditions = [...config.conditions];
                            if (plugins && Array.isArray(plugins) && plugins.length > 0) {
                                plugins.forEach(plugin => {
                                    if (plugin) {
                                        const p = this.plugins.find(plug => plug.name === plugin.name);
                                        var plCp = Object.assign({}, p);
                                        let pluginInst = new Plugin(Object.assign({}, plCp, { order: this.basicRule.plugins.length + 1 }, { settings: plugin.settings, dependencies: plugin.dependencies }));
                                        this.basicRule.plugins.push(pluginInst)
                                    }
                                });
                                this.stagePlugins();
                            }
                            this.rules = rules;
                            this.validate();
                            this.loading = false;
                        });
                });
    }

    selectPlugin(plugin) {
        if (plugin)
            this.activePlugin = plugin;
    }

    selectRulePlugin(plugin) {
        if (plugin)
            this.activePlugin = plugin;
    }

    ngOnDestroy() {
        if (this.configStateSub_n) {
            this.configStateSub_n.unsubscribe();
        }
        if (this.pluginsSub_n) {
            this.pluginsSub_n.unsubscribe();
        }
        if (this.proxySub_n) {
            this.proxySub_n.unsubscribe();
        }
        this.rules = [];

    }

    private get _valid(): boolean {
        let valid = this.rules && this.rules.length > 0;
        return valid;
    }

    stagePlugins() {
        this._store.dispatch(this._masterActions.setPluginsData({ plugins: this.basicRule.plugins, proxy: this.proxy, rules: this.rules, conditions: this.conditions, policy: this.policy }));
    }

    selectActiveRule(rule) {
        this.activeRule = rule;
    }

    onSubmit() {
        this.submitted = true;
        this._store.dispatch(this._validationActions.setValidity({ plugins: this._valid }))
        if (this._valid) {
            this.stagePlugins();
            this.next.next('preview');
        } else {
            this.validate();
        }
    }

    onSave() {
        this.submitted = true;
        this._store.dispatch(this._validationActions.setValidity({ plugins: this._valid }))
        if (this._valid) {
            this.stagePlugins();
            this.save.next('done');
        }
    }

    addRule() {
        this.rules.push(new Rule({
            path: '/*',
            plugins: [],
            policy:`[]
            
            
            
            
            `
        }));
        this.stagePlugins();
        this.validate();
    }

    editRule(rule: Rule) {
        this.underEdit = Object.assign({}, rule);
        this._modalRef = this.modalService
            .open(this.editPluginsContent, { windowClass: 'plugin-modal2' });
        this._modalRef.result.then((result) => {
            if (result) {
                console.log(result);
                rule.plugins = result.plugins;
                rule.proxy = result.proxy;
                rule.methods = result.methods;
                rule.conditions = result.conditions;
                rule.policy = result.policy;
                this.stagePlugins();
                this.underEdit = undefined;
            }
        });
        this.validate();
    }

    savePath(rule, value) {
        rule.path = value;
    }

    editCommon() {
        this.underEdit = Object.assign({}, this.basicRule, { proxy: this.proxy, base: true });
        this._modalRef = this.modalService
            .open(this.editPluginsContent, { windowClass: 'plugin-modal2' });
        this._modalRef.result.then((result) => {
            if (result) {
                console.log("COMMON:", result)
                this.basicRule.plugins = result.plugins;
                this.proxy = result.proxy; // todo here
                this.conditions = result.conditions; // todo here
                this.policy = result.policy;
                this.stagePlugins();
                this.underEdit = undefined;
            }
        });
        this.validate();
    }

    deleteRule(rule) {
        let ind = this.rules.indexOf(rule);
        this.rules.splice(ind, 1);
        this.stagePlugins();
        this.validate();
    }

    validate() {
        this._store.dispatch(this._validationActions.setValidity({ plugins: this._valid }));
        this.validation.next(this._valid);
    }
}