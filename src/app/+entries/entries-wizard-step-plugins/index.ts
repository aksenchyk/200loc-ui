export * from './entries-wizard-step-plugins.component';
export * from './entries-wizard-step-plugins-rule.component';
export * from './entries-option.component';

import { EntriesWizardStepPlugins } from './entries-wizard-step-plugins.component';
import { StepPluginsRule } from './entries-wizard-step-plugins-rule.component';
import { EntryOptionComponent } from './entries-option.component';

export const STEP_PLUGINS_COMPONENTS = [
    EntriesWizardStepPlugins,
    StepPluginsRule,
    EntryOptionComponent
]