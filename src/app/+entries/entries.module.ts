import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';

import { EntriesBaseComponent } from './entries-base';
import { ENTRIES_LIST_COMPONENTS } from './entries-list';
import { EntriesWizardBaseComponent } from './entries-wizard-base';
import { EntriesWizardStepGeneral } from './entries-wizard-step-general';
import { STEP_PLUGINS_COMPONENTS } from './entries-wizard-step-plugins';
import { EntriesWizardStepPreview } from './entries-wizard-step-preview';
import { HEALTHCHECK_COMPONENTS } from './entries-wizard-step-healthcheck';
import { FileUploadModule } from 'ng2-file-upload';

const ENTRIES_DECLARATIONS = [
  EntriesBaseComponent,
  EntriesWizardBaseComponent,
  EntriesWizardStepGeneral,
  ...HEALTHCHECK_COMPONENTS,
  ...STEP_PLUGINS_COMPONENTS,
  EntriesWizardStepPreview,
  ...ENTRIES_LIST_COMPONENTS
];

import { EntriesRoutingModule } from './entries.routing.module';
import { EntriesSharedModule } from './shared/entries.shared.module';
import { SharedModule } from '../shared';

@NgModule({
  declarations: [
    ...ENTRIES_DECLARATIONS    
  ],
  imports: [
    SharedModule,
    EntriesSharedModule,
    EntriesRoutingModule,
    PaginationModule.forRoot(),
    FileUploadModule    
  ]
})
export class EntriesModule { }
