import {
    Component, Input,
    Output, OnInit,
    EventEmitter, ViewChild
} from '@angular/core';
import {
    FormGroup, FormArray,
    Validators, FormBuilder,
    FormControl
} from '@angular/forms';
import { Observable } from 'rxjs';
import { DynamicForm } from '../../../../shared';
import { Plugin, ServiceApi } from '../../../../core';

@Component({
    selector: 'plugin-form',
    templateUrl: './plugin-form.tmpl.html',
    styles: [`
    
    .__container {
        position:relative
    }
    
    `]
})
export class PluginForm {
    form: FormGroup;
    pluginTemplate: any;
    dependenciesTemplate: any;
    deps: Array<any>;
    settingsValue: any;
    dependenciesValue: any = {};
    serviceConfigs: any = {};
    // valid: boolean = false;;

    @ViewChild('settingsForm') settForm: DynamicForm;
    @ViewChild('dependenciesForm') depsForm: DynamicForm;

    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();
    @Output() validation: EventEmitter<any> = new EventEmitter();
    loading: boolean = false;
    private _plugin: Plugin;

    no_dependencies: boolean;

    @Input()
    set plugin(pl: Plugin) {
        if (pl) {
            this._plugin = Object.assign({}, pl);
            // set template for plugin settings
            this.pluginTemplate = this._plugin.settingsTemplate;
            // set config and find available options for required service
            let fromDependencies = Observable
                .from([...this._plugin.dependenciesTemplate || []]);

            if (this._plugin.dependenciesTemplate && this._plugin.dependenciesTemplate.length === 0) {
                this.no_dependencies = true;
            } else {
                this.no_dependencies = false;
            }
            this.loading = true;
            fromDependencies
                .flatMap((dep) => this.serviceApi.getByType(this._plugin.dependenciesTemplate))
                .subscribe((serviceConfigs) => {
                    let tmpl = this._plugin.dependenciesTemplate
                        .reduce((options, v) => {
                            let srvs = serviceConfigs.filter((sc) => sc.serviceType == v)
                            options[v] = {
                                required: true,
                                label: v,
                                help: `${v} configuration`,
                                type: "select",
                                options: srvs.map((s) => ({ key: s.name, value: s.id }))
                            }
                            return options;
                        }, {});

                    this.dependenciesTemplate = tmpl;
                    // set dynamic form template for plugin's dependencies
                    this.dependenciesValue = this._plugin.dependencies ? { ...this._plugin.dependencies } : {};
                    this.loading = false;
                }, (err) => {
                    this.loading = false;
                });
            // set dynamic form template for regular settings
            this.settingsValue = Object.assign({}, this._plugin.settings);
        }
    }
    get plugin() {
        return this._plugin;
    }

    constructor(private _builder: FormBuilder,
        private serviceApi: ServiceApi) {
        this.form = this._builder.group({
            settings: [],
            dependencies: []
        });
    }

    ngAfterViewInit() {
        this.validation.emit(this.valid);
        this.form
            .valueChanges
            .subscribe((value) => {
                this.onChange.emit(value);
                this.validation.emit(this.valid);
            });
    }

    get valid() {
        let isValid = true;
        isValid = this.settForm ? this.settForm.valid : isValid;
        isValid = this.depsForm ? this.depsForm.valid : isValid;
        return isValid;
    }
}
