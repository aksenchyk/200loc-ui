import {
    Component, Input, QueryList, AfterContentInit, HostBinding, Output,
    ViewContainerRef, TemplateRef, ContentChildren, ViewRef, EventEmitter, ViewChild
} from '@angular/core';

import { ControlValueAccessor } from '@angular/forms'
import { Plugin } from '../../../../core';

@Component({
    selector: 'plugins-set',
    templateUrl: './pluginsSet.tmpl.html',
    styleUrls: [
        './component.scss'
    ]
})
export class PluginSet {
    @ViewChild('close') public close;
    @ViewChild('contentPlugins') content: TemplateRef<any>;
    _plugins: Array<Plugin> = [];
    _preparedForAdding: Plugin;

    @Input()
    public get plugins(): Array<Plugin> {
        return this._plugins;
    }
    public set plugins(v: Array<Plugin>) {
        this._plugins = v || [];
        if (this._plugins.length > 0) {
            this.activePlugin = this._plugins[0];
        }
    }
    @Input()
    all: Array<Plugin> = [];

    @Output('pluginsChange') emitter: EventEmitter<Array<Plugin>> = new EventEmitter<Array<Plugin>>();
    activePlugin;

    @Output()
    selected: EventEmitter<Plugin> = new EventEmitter<Plugin>();

    @Output()
    deleted: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    added: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    active: EventEmitter<any> = new EventEmitter<any>();


    private get _lastOrder(): number {
        var lastOrder = 0;
        if (this.plugins.length > 0) {
            lastOrder = this.plugins
                .reduce((prev: Plugin, current: Plugin) => {
                    return prev.order < current.order ? current : prev;
                }).order;
        }
        return lastOrder;
    }

    /**
        * Select plugin, action that sets active plugin and show it's settings form
        * 
        * @param {Plugin} [plugin]
        * @returns
        * 
        * @memberOf StepPlugins
        */
    selectPluginInPipe(plugin?: Plugin) {
        this.active.emit()

        this.plugins.forEach((p) => {
            p.active = false;
        })
        if (!plugin) {
            if (this.plugins.length > 0) {
                this.plugins[0].active = true;
                this.activePlugin = this.plugins[0];
            } else {
                return;
            }
        } else {
            console.log('Select plugin:', plugin)
            plugin.active = true;
            this.activePlugin = plugin;
        }
        this.selected.emit(plugin);
    }

    pluginUp(plugin: Plugin) {
        this.selectPluginInPipe(plugin);
        if (!this.isLast(plugin)) {
            var next = this.plugins[this.plugins.indexOf(plugin) + 1];
            plugin.order++;
            next.order--;
            this._sort();
        }
        this.selected.emit(plugin);
    }

    pluginDown(plugin: Plugin) {
        this.selectPluginInPipe(plugin);
        if (!this.isFirst(plugin)) {
            var prev = this.plugins[this.plugins.indexOf(plugin) - 1];
            plugin.order--;
            prev.order++;
            this._sort();
        }
        this.selected.emit(plugin);
    }

    pluginDelete(plugin) {
        var index = this.plugins.indexOf(plugin);
        this.plugins.splice(index, 1);
        if (this.plugins[0])
            this.selectPluginInPipe();
        this.deleted.emit();
    }

    isLast(plugin): boolean {
        return (plugin === this.plugins
            .reduce((prev: Plugin, current: Plugin) => {
                return prev.order < current.order ? current : prev;
            }));
    }

    isFirst(plugin): boolean {
        return (plugin === this.plugins
            .reduce((prev: Plugin, current: Plugin) => {
                return prev.order > current.order ? current : prev;
            }));
    }

    private _sort() {
        this.plugins.sort((a, b) => {
            return a.order - b.order;
        });
    }

    public selectLast() {
        if (this.plugins.length > 0) {
            let pluginToSelect = this.plugins[this.plugins.length - 1];
            this.selectPluginInPipe(pluginToSelect);
        }
    }
}
