import {
    Component, Input,
    Output, OnInit,
    EventEmitter, ViewChild, TemplateRef
} from '@angular/core';
import {
    FormGroup, FormArray,
    Validators, FormBuilder,
    FormControl
} from '@angular/forms';
import { Observable } from 'rxjs';

import { Proxy,/* IProxy, IRule,*/ Rule } from '../../../../core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'rule-form',
    templateUrl: './proxy-form.tmpl.html',
    styleUrls: ['./proxy-settings-form.scss']
})
export class ProxyForm {
    proxyForm: FormGroup;
    methodsForm: FormGroup;

    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();
    @Output() ruleChange: EventEmitter<Rule> = new EventEmitter<Rule>();
    @Output() validation: EventEmitter<any> = new EventEmitter();
  
    private _rule: Rule = {};
    @Input()
    set rule(value: Rule) {
        if (value) {
            this.proxy = value.proxy;
            this.methods = value.methods;
            this.methods = value.methods;
            this._rule = value;
          //  this.policy = value.policy || "{}"
        }
    }
    get rule() {
        return this._rule;
    }
    private _proxy: Proxy = {};

    initComplete: false;
    @Input()
    set proxy(value: Proxy) {
        if (value) {
            if (!value.active) {
                Object.keys(this.proxyForm.controls).forEach((c) => {
                    if (c != 'active') this.proxyForm.controls[c].disable();
                })
            } else {
                Object.keys(this.proxyForm.controls).forEach((c) => {
                    if (c != 'active') this.proxyForm.controls[c].enable();
                })
            }
            this._proxy = Object.assign({}, value);
        }

    }
    get proxy() {
        return this._proxy;
    }
    private _methods = [];

    @Input()
    set methods(value) {
        if (value && Array.isArray(value)) {
            this._methods = [...value];
        }
    }
    get methods(): Array<string> {
        return this._methods;
    }
    
    @Input()
    active: boolean;

    @Input()
    base: boolean;

    options: Array<any> = [
        { name: 'GET', description: 'GET' },
        { name: 'POST', description: 'POST' },
        { name: 'PUT', description: 'PUT' },
        { name: 'DELETE', description: 'DELETE' },
        { name: 'HEAD', description: 'HEAD' },
        { name: 'PATCH', description: 'PATCH' }
    ];

    constructor(private _builder: FormBuilder) {
        this.proxyForm = new FormGroup({
            active: new FormControl({ value: false }),
            target: new FormControl({ value: '' }, Validators.required),
            prependPath: new FormControl({ value: false }),
            secure: new FormControl({ value: false }),
            ignorePath: new FormControl({ value: false }),
            changeOrigin: new FormControl({ value: false }),
            toProxy: new FormControl({ value: false }),
            xfwd: new FormControl({ value: false }),
            auth: new FormControl({ value: '' }),
            ws: new FormControl({ value: false }),
            proxyTimeout: new FormControl({ value: 3000 }),
        });
        this.methodsForm = new FormGroup({
            methods: new FormControl({ value: [] })
        });
    }

    ngAfterViewInit() {
        this.proxyForm
            .valueChanges
            .distinctUntilChanged()
            .subscribe((value) => {
                this.rule.proxy = this.proxy;
                this.rule.proxy.proxyTimeout = +this.rule.proxy.proxyTimeout || 3000;
                this.ruleChange.emit(this.rule);
                // this.validation.emit(this.valid);
            });
        this.methodsForm
            .valueChanges
            .distinctUntilChanged()
            .subscribe((value) => {
                this.rule.methods = value.methods;
                this.ruleChange.emit(this.rule);
                // this.validation.emit(this.valid);
            });
    }

    get valid() {
        let isValid = true;
        isValid = this.proxyForm ? this.proxyForm.valid : isValid;
        return isValid;
    }     
}
