import {
    Component, Input, QueryList, AfterContentInit, HostBinding,
    ViewContainerRef, TemplateRef, ContentChildren, ViewRef, EventEmitter, Output
} from '@angular/core';
import { UiPane } from './step-tabs-pane';

@Component({
    selector: 'ui-tabs',
    templateUrl: './step-tabs.tmpl.html',
    styleUrls: [
        './tabs.scss'
    ]
})
export class UiTabs {
    @ContentChildren(UiPane) panes: QueryList<UiPane>;
    currentPane: UiPane;
    @Input()
    default: string;
    @Output()
    save: EventEmitter<any> = new EventEmitter<any>();
    ngAfterContentInit() {
        if (this.panes) {
            this.default
                ? this.currentPane = this.panes.toArray().find((p: UiPane) => p.id == this.default)
                : this.currentPane = this.panes.first;
            this.currentPane.active = true;
        }
    }
    goTo(id) {
        if (this.panes) {
            if (this.currentPane) {
                this.currentPane.visited = true;
            }
            this.panes.toArray().forEach((p: UiPane) => p.active = false);
            this.currentPane = this.panes.toArray().find((p: UiPane) => p.id == id);
            this.currentPane.active = true;
        }
    }
    isSomeInvalid() {
        return this.panes.some((p) => !p.valid);       
    }
}