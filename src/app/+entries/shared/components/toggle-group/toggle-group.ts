import {
    Component, Self,
    EventEmitter, Output,
    ViewChild, Input, Optional
} from '@angular/core';
import {
    NgControl, NgModel,
    ControlValueAccessor
} from '@angular/forms';

@Component({
    selector: 'toggleGroup[ngModel]',
    templateUrl: './toggle-group.tmpl.html',
    styleUrls: ['./toggle-group.scss']
})

export class ToggleGroup implements ControlValueAccessor {
    private _selectedOptions = [];

    private _options: Array<any> = [];
    @Input()
    set options(op) {
        if (op) {
            this._options = op;
            this.__update();
        }

    }
    get options(): any {
        return this._options;
    }

    set selectedOptions(arrayOfSelected) {

    }

    get selectedOptions(): any {
        return this._selectedOptions;
    }

    constructor( @Optional() @Self() private ngControl: NgControl) {
        if (ngControl) ngControl.valueAccessor = this;
    }

    onCheck(index) {
        this.options[index].active = !this.options[index].active;
        let activeOptions = this.options.filter((option) => option.active);
        let newValue = activeOptions.map((option) => option.name);
        if (this.ngControl) this.ngControl.viewToModelUpdate(newValue);
        this.onChange(newValue)
    }

    __update() {
        if (this._selectedOptions && this._selectedOptions.length > 0 && this._options && this._options.length > 0) {
            this._options = this._options.map((option) => {
                return this._selectedOptions.indexOf(option.name) > -1
                    ? Object.assign(option, { active: true })
                    : Object.assign(option, { active: false })
            });
        }
    }

    /**
     * ControlValueAccessor
     */
    onChange = (_: any) => {
    };
    onTouched = () => {
    };
    writeValue(value) {
        if (value) {
            this._selectedOptions = value;
            this.__update();
        }

    }
    registerOnChange(fn): void {
        this.onChange = fn;
    }
    registerOnTouched(fn): void {
        this.onTouched = fn;
    }
}
