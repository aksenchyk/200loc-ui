import {
    Component, OnInit, AfterViewInit, Output,
    Input, EventEmitter, OnDestroy, Host, Optional,
    trigger, state, style, animate, transition
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Config, HealthCheck } from '../../core/models';

import { Observable, Subscription } from 'rxjs';
import { HealthcheckApi, ApiConfig, ServiceApi, ServiceStatus, LoopBackAuth } from '../../core'
import { Store } from '@ngrx/store';
import { AppState, getConfigState } from '../../core/reducers';
import { MasterActions, ValidationActions } from '../../core/actions';

@Component({
    selector: 'healthcheck-details',
    templateUrl: "./healthcheck-details.component.tmpl.html",
    styleUrls: ['./healthcheck-details.scss']   
})
export class HealthcheckDetailsComponent {
   
    selected$: Observable<any>;
    healthRuleForm: FormGroup;
    ID: string;
    private _healthCheckRule = {};
    @Input()
    public get healthCheckRule(): any {
        return this._healthCheckRule;
    }

    public set healthCheckRule(v: any) {
        if (v)
            this._healthCheckRule = v;
    }


    constructor(
        private _store: Store<AppState>,
        private _masterActions: MasterActions,
        private route: ActivatedRoute,
        private router: Router,
        private _validationActions: ValidationActions,
        private _healthCheckService: HealthcheckApi,
        private _fb: FormBuilder) {
        this.healthRuleForm = this._fb.group({
            target: [],
            withPath: [],
            method: [],
            interval: []
        });

    }

    ngOnInit() {
        // this.selected$ = 
        this.route.paramMap
            .switchMap((params: ParamMap) => {
                // (+) before `params.get()` turns the string into a number
                //this.selectedId = +params.get('id');
                this.ID = params.get('id');
                if (!this.ID) {
                    return Observable.of(new HealthCheck());
                }
                return this._healthCheckService.findById(this.ID);
            })
            .subscribe(result => {
                this.healthCheckRule = result;
                console.log(result)
            }, (err) => {
                console.log(err);
            });      
    }

    onSave(event) {
        let hc = this.healthRuleForm.value;
        //*ngIf="!ID"
        let sb;
        if (!this.ID) {
            sb = this._healthCheckService
                .create(hc);
        } else {
            sb = this._healthCheckService
                .update(this.ID, hc)
        }
        sb
            .subscribe((result) => {
                this.router.navigate(["/health"])
            }, (err) => {
                console.log(err);
            });
    }
  
}

