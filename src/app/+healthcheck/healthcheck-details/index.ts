export * from './healthcheck-details.component';

import { HealthcheckDetailsComponent } from './healthcheck-details.component';

export const HEALTHCHECK_DETAILS_COMPONENTS = [
    HealthcheckDetailsComponent
]