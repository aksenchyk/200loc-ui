import { Input, Output, Component, OnInit, OnDestroy, trigger, state, transition, style, animate, ViewChild, ElementRef, NgZone, EventEmitter, TemplateRef } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { HealthcheckApi, HealthCheck } from '../../core'

import { AppState, getHealth } from '../../core/reducers';
import { Store } from '@ngrx/store';

@Component({
    selector: 'healthcheck-list-item',
    templateUrl: './healthcheck-list-item.component.tpml.html',
    styleUrls: ['./healthcheck-list-item.scss'],
    animations: [
        trigger('initial', [
            state('start', style({
                visibility: 'hidden',
                opacity: 0
            })),
            state('complete', style({
                visibility: 'visible',
                opacity: 1
            })),
            transition('start => complete', [
                animate('300ms linear')
            ])
        ])
    ]
})
export class HealthcheckListItemComponent implements OnInit, OnDestroy {

    @Input()
    item: HealthCheck;
    @Output()
    onDelete: EventEmitter<string> = new EventEmitter();

    updateIntervalHandler: any;
    constructor(
        private _healthcheckApi: HealthcheckApi,
        private _store: Store<AppState>
    ) { }

    ngOnInit() {

    }

    ngOnDestroy() {

    }
}
