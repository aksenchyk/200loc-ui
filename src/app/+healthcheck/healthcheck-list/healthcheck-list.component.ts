import { Component, OnInit, OnDestroy, trigger, state, transition, style, animate, ViewChild, ElementRef, NgZone, EventEmitter, TemplateRef } from '@angular/core';
import { LoaderComponent, LocPagerComponent } from '../../shared/components';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { HealthcheckApi, ApiConfig, ServiceApi, ServiceStatus, LoopBackAuth, HealthCheck } from '../../core'
import { FormControl } from '@angular/forms';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap'
import { PagerComponent } from 'ngx-bootstrap';
import { saveAs } from "file-saver";
import { CoreConfig } from '../../core/core.config';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppState, getHealth } from '../../core/reducers';
import { Store } from '@ngrx/store';

interface IQuery {
    name?: string;
    page?: number;
}

@Component({
    selector: 'healthcheck-list',
    templateUrl: './healthcheck-list.component.tmpl.html',
    styleUrls: ['./healthcheck-list.scss'],
    animations: [
        trigger('initial', [
            state('start', style({
                visibility: 'hidden',
                opacity: 0
            })),
            state('complete', style({
                visibility: 'visible',
                opacity: 1
            })),
            transition('start => complete', [
                animate('300ms linear')
            ])
        ])
    ]
})
export class HealthcheckListComponent implements OnInit, OnDestroy {
    @ViewChild('searchInput') searchInput: ElementRef;
    @ViewChild(PagerComponent) pager: PagerComponent;
    @ViewChild(LocPagerComponent) locPager: LocPagerComponent;
    @ViewChild('importPreview') content: TemplateRef<any>;
    @ViewChild('uploadEl') uploadElRef: ElementRef;

    ////////////////////////////////

    healthChecks: Array<HealthCheck>;

    /////////////////////////////

    configs: Array<any> = [];
    loading: boolean = false;
    sidebarActive: boolean = false;
    serviceStatusArray: Array<ServiceStatus> = [];
    initialComplete: boolean = false;
    state: string = 'start';
    count: number;
    searchParams: IQuery = {};
    maxItems: number = 10;
    showMenu: boolean = false;
    searchQuery: string;
    searchControl: FormControl = new FormControl();
    public uploader: FileUploader;
    modalRef: NgbModalRef;
    importData: { routes: any[] } = { routes: [] };

    updateIntervalHandler: any;
    constructor(      
        private router: Router,
        private route: ActivatedRoute,
        private _serviceApi: ServiceApi,
        private _authService: LoopBackAuth,
        private _ngZone: NgZone,
        private modalService: NgbModal,
        private _healthcheckApi: HealthcheckApi,
        private _store: Store<AppState>

    ) {
        let header: any = { 'name': "Authorization", 'value': _authService.user ? _authService.user.accessToken : '' };
    }

    ngOnInit() {

        this._healthcheckApi
            .find()
            .subscribe(result => {
                this.healthChecks = result;
            }, (err) => {
                console.log(err);
            })
    }

    ngAfterViewInit() {

        if (this.locPager)
            this.locPager
                .pageChanged
                .distinctUntilChanged()
                .do(() => this.loading = true)
                .map((value) => { return { page: value.page } })
                .subscribe((q: IQuery) => {

                });
    }

    ngOnDestroy() {
        if (this.updateIntervalHandler) {
            clearInterval(this.updateIntervalHandler);
        }
    }

    removeItem(id) {
        this._healthcheckApi.deleteById(id)
            .subscribe(res => {
                let ind = this.healthChecks.findIndex((el)=>el._id == id);
                this.healthChecks.splice(ind, 1);
            }, (err) => {
                console.log(err);
            });
    }

    onCreateNewHealthCheck(event) {
        this.router.navigate(["details", {}], { queryParams: {}, relativeTo: this.route })
    }
}
