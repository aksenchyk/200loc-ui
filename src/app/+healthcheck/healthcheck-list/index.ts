export * from './healthcheck-list.component';
export * from './healthcheck-list-item.component';

import { HealthcheckListComponent } from './healthcheck-list.component';
import { HealthcheckListItemComponent } from './healthcheck-list-item.component';

export const HEALTHCHECK_LIST_COMPONENTS = [
    HealthcheckListComponent,
    HealthcheckListItemComponent
]