import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';

import { HealthcheckBaseComponent } from './healthcheck-base';
import { HEALTHCHECK_LIST_COMPONENTS } from './healthcheck-list';
import { HEALTHCHECK_DETAILS_COMPONENTS } from './healthcheck-details';

const HEALTHCHECK_DECLARATIONS = [
    HealthcheckBaseComponent,
    ...HEALTHCHECK_LIST_COMPONENTS,
    ...HEALTHCHECK_DETAILS_COMPONENTS
];

import { HealthcheckRoutingModule } from './healthcheck.routing.module';
import { SharedModule } from '../shared';

@NgModule({
    declarations: [
        ...HEALTHCHECK_DECLARATIONS
    ],
    imports: [
        SharedModule,
        HealthcheckRoutingModule,
        PaginationModule.forRoot()
    ]
})
export class HealthcheckModule { }
