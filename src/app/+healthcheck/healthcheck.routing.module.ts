import { HealthcheckBaseComponent } from './healthcheck-base';
import { HealthcheckListComponent } from './healthcheck-list';
import { HealthcheckDetailsComponent } from './healthcheck-details';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsAuthenticatedGuard } from '../core';

export const routes: Routes = [
    {
        path: '',
        component: HealthcheckBaseComponent,
        canActivate: [IsAuthenticatedGuard],
        children: [
            {
                path: '',
                component: HealthcheckListComponent
            },
            {
                path: 'details',
                component: HealthcheckDetailsComponent
            }
        ]
    },

];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HealthcheckRoutingModule { }
