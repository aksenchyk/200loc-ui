import { Component, OnInit, ViewChild, AfterViewInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceConfig, ServiceApi } from '../../core';
import { Observable } from 'rxjs';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    templateUrl: './services-list-summary.component.html',
    styleUrls: [`./services-list.scss`]
})
export class ServicesListSummaryComponent implements AfterViewInit {
    serviceName: string;
    serviceType: string;
    serviceTemplate: any;
    summary: any = { message: [], general: [], configuration: [] };
    serviceConfig: ServiceConfig;
    currentService: any = {};
    configured: boolean = false;
    modalRef: NgbModalRef;
    loading: boolean = false;;
    submitted: boolean = false;
    @ViewChild('close') public close;
    @ViewChild('serviceModalContent') content: TemplateRef<any>;

    constructor(
        private activeRoute: ActivatedRoute,      
        private serviceApi: ServiceApi,
        private modalService: NgbModal,
        private router: Router
    ) { }

    ngOnInit() {
        this.serviceName = this.activeRoute.snapshot.queryParams['name'];
        this.serviceType = this.activeRoute.snapshot.queryParams['type'];
        this.loading = true;
    }

    ngAfterViewInit() {
        this.serviceApi
            .getServiceSummaryByName(this.serviceName)
            .subscribe((res) => {
                let summary: any = {};
                summary.message = res.message && Array.isArray(res.message) ? res.message : [];
                summary.general = res.general && Array.isArray(res.general) ? res.general : [];
                summary.configuration = res.configuration && Array.isArray(res.configuration) ? res.configuration : [];
                this.summary = summary;
                this.loading = false;
            }, (err) => {
                console.log(err);
                this.loading = false;
            });
    }
}
