import { NgModule } from '@angular/core';

import { ServicesTypesListComponent } from './services-list-types';
import { ServicesListSummaryComponent } from './services-list-summary';
import { ServicesBaseComponent } from './services-base';
import { ServicesRoutingModule } from './services.routing.module';

import { SharedModule } from '../shared';

const SERVICES_MODULE_DECLARATIONS = [
  ServicesTypesListComponent,
  ServicesListSummaryComponent,
  ServicesBaseComponent
];

@NgModule({
  declarations: [
    ...SERVICES_MODULE_DECLARATIONS,
  ],
  imports: [
    SharedModule,
    ServicesRoutingModule
  ]
})
export class ServicesModule { }
