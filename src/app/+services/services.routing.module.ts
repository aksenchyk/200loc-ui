import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsAuthenticatedGuard } from '../core';

import { ServicesTypesListComponent } from './services-list-types';
import { ServicesListSummaryComponent } from './services-list-summary';
import { ServicesBaseComponent } from './services-base';

export const routes: Routes = [
  {
    path: '',
    component: ServicesBaseComponent,
    canActivate: [IsAuthenticatedGuard],
    children: [
      {
        path: 'config',
        component: ServicesListSummaryComponent
      },
      {
        path: '',
        component: ServicesTypesListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes) /*use root router services*/ ],
  exports: [RouterModule],
})
export class ServicesRoutingModule { }

