import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgUploaderModule } from 'ngx-uploader';

import { AppComponent } from './app.component';

import { AUTHENTICATION_COMPONENTS } from './auth';
import { NotFoundComponent } from './notFound';

import { CoreModule } from './core';
import { AppRoutingModule } from './app.routing.module';

import { SharedModule } from "./shared";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Redux
import { StoreModule } from '@ngrx/store';
import reducer from './core/reducers';

import 'brace';
import 'brace/theme/eclipse';
import 'brace/mode/json';

// main styles
import '../theme/styles.scss';

import {
  LocationStrategy,
  HashLocationStrategy
} from '@angular/common';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    ...AUTHENTICATION_COMPONENTS,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    SharedModule.forRoot(),
    NgbModule.forRoot(),
    StoreModule.provideStore(reducer),
    CoreModule.forRoot(),
    AppRoutingModule,
    BrowserAnimationsModule
  ],

  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) { }
}

/**
 * TODO LIST:
 * - see https://github.com/mpalourdio/ng-http-loader for spinner solution using ng4+ interceptors
 */