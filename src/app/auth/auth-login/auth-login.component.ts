import {
  Component, OnInit, AfterViewInit,
  TemplateRef,
  ElementRef, ViewChild, ViewChildren,
  QueryList, HostListener, Renderer, Inject
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserApi, LoopBackAuth, UserActions, AppState } from '../../core';
import { Store } from '@ngrx/store';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'auth-login',
  templateUrl: './auth-login.component.html'
})

export class LoginComponent {
  @ViewChild('close') public close;
  @ViewChild('content') content: TemplateRef<any>;
  //signInForm: FormGroup;
  error: string;
  modalRef: NgbModalRef;
  name: string = '';
  password: string = '';
  success: string;

  constructor(
    private element: ElementRef,
    @Inject(DOCUMENT) private _document: any,
    private router: Router,
    private _location: Location,
    private modalService: NgbModal,
    private _renderer: Renderer,
    private _builder: FormBuilder,
    private _store: Store<AppState>,
    private _userActions: UserActions,
    private userApi: UserApi,
    private authService: LoopBackAuth,
    private route: ActivatedRoute) {

  }
  ngOnInit() {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.modalRef = this.modalService
        .open(this.content, { windowClass: 'auth-modal', backdrop: 'static' });

      this.modalRef.result.then((result) => {
        this._location.back();
        this.__destroy();
      }, (reason) => {
        this._location.back();
        this.__destroy();
      });
      let body = this._document.getElementsByTagName('body')[0];
      if (body) {
        this._renderer.setElementClass(body, 'blurred', true);
      }
    }, 0);
  }

  __destroy() {
    let body = this._document.getElementsByTagName('body')[0];
    if (body) {
      this._renderer.setElementClass(body, 'blurred', false);
    }
  }

  goBack() {
    this._location.back();
    this.__destroy();
  }

  onSubmit() {
    this.userApi
      .login({ username: this.name, password: this.password, remember: true })
      .subscribe(
      (data) => {
        this.onSuccess(data);
      },
      (err) => {
        this.onError(err);
      });
  }

  onSuccess(data) {
    const userdata = {
      accessToken: data.id,
      username: data.user.username
    }
    this._store.dispatch(this._userActions.login(userdata));
    const from = this.route.snapshot.queryParams['from'] || '/';
    this.router.navigate([from]);
    if (this.modalRef) {
      this.modalRef.close();
    }
    this.__destroy();
  }

  onError(err) {
    this.error = 'Login failed';
  }
}