import {
    Component, OnInit, AfterViewInit,
    TemplateRef,
    ElementRef, ViewChild, ViewChildren,
    QueryList, HostListener, Renderer, Inject
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';

import { UserApi, LoopBackAuth, UserActions, AppState, getAuthenticationState } from '../../core';
import { Store } from '@ngrx/store';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
    selector: 'auth-change-password',
    templateUrl: './auth-password-change.component.tmpl.html'
})

export class ChangePasswordComponent {
    @ViewChild('close') public close;
    @ViewChild('content') content: TemplateRef<any>;

    changeForm: FormGroup;
    error: string;
    submitted: boolean = false;
    username: string;

    modalRef: NgbModalRef;
    name: string = '';
    password: string = '';
    success: string;

    constructor(
        private element: ElementRef,
        private router: Router,
        private _location: Location,
        private modalService: NgbModal,
        private _renderer: Renderer,
        builder: FormBuilder,
        private _store: Store<AppState>,
        private _userActions: UserActions,
        private userApi: UserApi,
        private authService: LoopBackAuth,
        @Inject(DOCUMENT) private _document: any,
        private route: ActivatedRoute) {
        this.changeForm = builder.group({
            username: [''],
            oldpassword: [''],
            newpassword: [''],
            confirmnewpassword: ['']
        });
    }

    ngOnInit() {
        this._store.let(getAuthenticationState())
            .subscribe((state) => {
                console.log(state.user);
                this.username = state.user ? state.user.username : ''
            })
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.modalRef = this.modalService
                .open(this.content, { windowClass: 'auth-modal', backdrop: 'static' });
            this.modalRef.result.then((result) => {
                this._location.back();
                this.__destroy();
            }, (reason) => {
                this._location.back();
                this.__destroy();
            });
            let body = this._document.getElementsByTagName('body')[0];
            if (body) {
                this._renderer.setElementClass(body, 'blurred', true);
            }
        }, 0);
    }

    __destroy() {
        let body = this._document.getElementsByTagName('body')[0];
        if (body) {
            this._renderer.setElementClass(body, 'blurred', false);
        }
    }

    onSubmit(value) {
        console.log(value);
        this.submitted = true;
        if (this.changeForm.valid) {
            this.userApi
                .change({
                    username: value.username,
                    password: value.oldpassword,
                    newPassword: value.newpassword
                })
                .subscribe((data) => {
                    this.modalRef.close();
                    this._store.dispatch(this._userActions.logout());
                    this.authService.logout();
                    this.router.navigate(['auth']);
                    this.__destroy();
                }, (err) => {
                    this.error = err.message || 'Unexpected error';
                });
        }
    }

    cancel() {
        this.modalRef.close();
        this.__destroy();
    }
}

