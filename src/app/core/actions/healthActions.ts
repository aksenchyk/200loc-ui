import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

@Injectable()
export class HealthActions {
    static SET_HEALTH = '[HEALTH] SET HEALTH';
    setHealth(entriesHEalth: Array<any>): Action {
        return {
            type: HealthActions.SET_HEALTH,
            payload: entriesHEalth
        };
    }
}