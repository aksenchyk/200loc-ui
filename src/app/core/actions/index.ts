import { MasterActions } from './masterActions';
import { DefaultsActions } from './defaultsActions';
import { ValidationActions } from './validationActions';
import { ConfigActions } from './configActions';
import { UserActions } from './user.actions';
import { HealthActions } from './healthActions';
import { StatsActions } from './statsActions';
import { NodesActions } from './nodesActions';

export * from "./defaultsActions";
export * from "./masterActions";
export * from "./validationActions";
export * from "./configActions";
export * from "./user.actions";
export * from './healthActions';
export * from './statsActions';
export * from './nodesActions';

export const APP_ACTIONS = [
    MasterActions,
    DefaultsActions,
    ValidationActions,
    ConfigActions,
    UserActions,
    HealthActions,
    StatsActions,
    NodesActions
];