
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

@Injectable()
export class NodesActions {
    static SET_NODES = '[NODES] SET NODES';
    setNodes(nodes: Array<any>): Action {
        return {
            type: NodesActions.SET_NODES,
            payload: nodes
        };
    }
}