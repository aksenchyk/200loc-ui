import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

@Injectable()
export class StatsActions {
    static SET_STATS = '[STATS] SET STATS';
    setStats(entriesStats: any): Action {
        return {
            type: StatsActions.SET_STATS,
            payload: entriesStats
        };
    }
}