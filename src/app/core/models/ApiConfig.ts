/* tslint:disable */
import { Plugin } from './plugin';
import { ServiceStatus } from './ServiceStatus';
import { Rule } from './Rule';
import { Proxy } from './Proxy'

export interface ApiConfigInterface {
  name: string;
  public: boolean;
  description?: string;
  entry: string;
  methods: Array<string>;
  plugins?: Array<Plugin>;
  id?: any;
  ok?: any;
  errors?: Array<any>;
  rules?: Rule[];
  active?: boolean;
  proxy?: Proxy;
}

export class ApiConfig implements ApiConfigInterface {
  name: string;
  public: boolean;
  description: string;
  entry: string;
  methods: Array<string> = ["GET", "POST"];
  plugins: Array<Plugin> = [];
  id: any;
  ok: any;
  active: boolean;
  rules: Rule[] = [];
  errors: Array<any> = [];
  proxy: Proxy;

  constructor(instance?: ApiConfig) {
    Object.assign(this, instance);
  }
}
