export interface IEntryOption {
    source?: string;
    operation?: string;
    key?: boolean;
    value: string;
}

export class EntryOption {
    source: string;
    operation: string;
    key: boolean;
    value: string;
    constructor(instance?: IEntryOption) {
        Object.assign(this, instance);
    }
}
