export interface IHealthCheck {
    target?: string;
    withPath?: string;
    active?: boolean;
    method: string;
    interval: number;
    _id?: string;
}

export class HealthCheck {
    target: string;
    withPath: string;
    active: boolean;
    method: string;
    interval: number;
    _id: string;
    constructor(instance?: IHealthCheck) {
        Object.assign(this, instance);
    }
}

export class Health {
    active: boolean;
    interval: number;
    healthCheck: HealthCheck[] = [];
    constructor(instance?: any) {
        Object.assign(this, instance);
    }
}
