
export interface IProxy {
    target?: string;
    active?: boolean;
    prependPath?: boolean;
    secure?: boolean;
    ignorePath?: boolean;
    changeOrigin?: boolean;
    toProxy?: boolean;
    xfwd?: boolean;
    auth?: string;
    ws?: boolean;
    proxyTimeout?: number;
}

export class Proxy implements IProxy {
    target?: string;
    active?: boolean;
    prependPath?: boolean;
    secure?: boolean;
    ignorePath?: boolean;
    changeOrigin?: boolean;
    toProxy?: boolean;
    xfwd?: boolean;
    auth?: string;
    ws?: boolean;
    proxyTimeout?: number;
    constructor(instance?: IProxy) {
        Object.assign(this, instance);
    }
}
