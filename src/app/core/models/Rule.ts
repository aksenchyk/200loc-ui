import { Plugin } from './plugin';
import { Proxy } from './Proxy';
import { EntryOption } from './Condition';

export interface IRule {
    path?: string;
    plugins?: Plugin[];
    proxy?: Proxy;
    methods?: string[];
    base?: string;
    conditions?: EntryOption[];
    policy?: string;
}

export class Rule {
    path?: string;
    plugins?: Plugin[];
    proxy?: Proxy;
    methods?: string[];
    conditions?: EntryOption[];
    base?: string;
    policy?: string;
    constructor(instance?: IRule) {
        Object.assign(this, instance);
    }
}
