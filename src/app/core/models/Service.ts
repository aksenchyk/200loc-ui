/* tslint:disable */
export interface IService {
    name?: string;
    description?: string;
    settings?: any;
    serviceId?: string;
    id?: number;
    serviceType?: string;
}

export class Service {
    name?: string;
    description?: string;
    settings?: any;
    serviceId?: string;
    id?: number;
    serviceType?: string;
    constructor(instance?: IService) {
        Object.assign(this, instance);
    }
}
