export interface IJsonSchema {
    id?: string;
    example?: string;
    $schema?: string;
    $ref?: string;
    title?: string;
    description?: string;
    multipleOf?: number;
    maximum?: number;
    exclusiveMaximum?: boolean;
    minimum?: number;
    exclusiveMinimum?: boolean;
    maxLength?: number;
    minLength?: number;
    pattern?: string;
    additionalItems?: boolean | IJsonSchema;
    items?: IJsonSchema;
    maxItems?: number;
    minItems?: number;
    uniqueItems?: boolean;
    maxProperties?: number;
    minProperties?: number;
    required?: string[];
    additionalProperties?: boolean | IJsonSchema;
    definitions?: {
        [name: string]: IJsonSchema
    };
    properties?: {
        [name: string]: IJsonSchema
    };
    patternProperties?: {
        [name: string]: IJsonSchema
    };
    dependencies?: {
        [name: string]: IJsonSchema | string[]
    };
    'enum'?: any[];
    type?: string;
    format?: string;
    default?: boolean;
    allOf?: IJsonSchema[];
    anyOf?: IJsonSchema[];
    oneOf?: IJsonSchema[];
    not?: IJsonSchema;
    xml?: { name: string, wrapped: boolean };
}

//import {ItemsObject, ReferenceObject} from './apidoc';
import { SwaggerUtils } from '../utils/swagger.utils';

//import * as _ from 'lodash';

const TYPE_FILE = 'file';
const TYPE_DATE = 'date';
const PATH_PARAM = 'path';
const QUERY_PARAM = 'query';
const BODY_PARAM = 'body';
const FORM_PARAM = 'formData';
const HEADER_PARAM = 'header';

const HTTP_METHOD_PATCH = 'PATCH';
const HTTP_METHOD_POST = 'POST';
const HTTP_METHOD_PUT = 'PUT';
const HTTP_METHOD_GET = 'GET';
const HTTP_METHOD_DELETE = 'DELETE';

const METHOD_CLASS: Object = {
    GET: 'grey lighten-1',
    POST: 'teal lighten-2',
    PUT: 'yellow darken-2',
    DELETE: 'red lighten-2',
    PATCH: 'light-blue lighten-2',
    HEAD: 'pink lighten-2'
};


const APPLICATION_FORM_URL_ENCODED = 'app/x-www-form-urlencoded';
const MULTIPART_FORM_DATA = 'multipart/form-data';
const APPLICATION_JSON = 'application/json';
const APPLICATION_XML = 'application/xml';

export class ParametersDefinitionsObject {
    [index: string]: ParameterObject;
}

export class OperationObject {
    name: string;
    path: string;
    tags: string[];
    summary: string;
    description: string;
    externalDocs: ExternalDocumentationObject;
    operationId: string;
    consumes: string[];
    produces: string[];
    parameters: (ParameterObject)[];
    responses: Array<ResponsesObject>;
    schemes: string[];
    deprecated: boolean;
    security: SecurityRequirementObject[];
    originalData: any;
    dataJson: string;
    patchJson: string;
    consume: { value?: string, selected: string };
    produce: { value?: string, selected: string };
    slug: string;
    chartColor: string;
    constructor(path?: string, method?: string, _opObj?: any) {
        this.responses = [];
        this.parameters = [];
        this.produces = [];
        this.consumes = [];
        this.path = path;
        this.produce = { selected: APPLICATION_JSON };
        this.consume = { selected: APPLICATION_JSON };
        if (method) {
            this.name = method.toUpperCase();
        }
        if (_opObj) {
            Object.assign(this, _opObj);
            this.slug = btoa(this.name + this.path + this.operationId);
            if (_opObj.externalDocs) {
                this.externalDocs = new ExternalDocumentationObject(_opObj.externalDocs);
            }
            if (_opObj.responses) {
                this.responses = [];
                Object.keys(_opObj.responses).forEach((code: string) => {
                    this.responses.push(new ResponsesObject(code, _opObj.responses));
                });
            }
            if (_opObj.parameters) {
                this.parameters = [];
                _opObj.parameters.forEach((param: any) => {
                    this.parameters.push(new ParameterObject(param));
                });
            }
            if (_opObj.produces && !(this.produces.length == 0)) {
                this.produce = { selected: this.produces[0] };
            }
            if (_opObj.consumes && !(this.consumes.length == 0)) {
                this.consume = { selected: this.consumes[0] };
            }
        }
    }
    getMethodClass(): string {
        if (this.name) {
            return METHOD_CLASS[this.name];
        }
    }
    getResponseByCode(code: string): ResponseObject {
        let respObj: ResponsesObject = this.responses.find((resp: ResponsesObject) => {
            return resp.code === code;
        });

        if (respObj) {
            return respObj.response;
        }
    }
    getRequestUrl(onlyParameters = false): string {
        let url: string = !onlyParameters ? this.path : '';

        if (this.parameters.length > 0) {
            this.parameters.forEach((param: ParameterObject) => {
                if (param.value && param.value.selected) {
                    if (param.isPathParam()) {
                        url = url.replace(new RegExp('{' + param.name + '}'), param.value.selected);
                    } else if (param.isQueryParam()) {
                        url += url.indexOf('?') === -1 ? '?' + param.name + '='
                            + param.value.selected : '&' + param.name + '=' + param.value.selected;
                    }
                }
            });
        }
        return url;
    }
    isPatchMethod(): boolean {
        return this.name === HTTP_METHOD_PATCH;
    }
    isPostMethod(): boolean {
        return this.name === HTTP_METHOD_POST;
    }
    isPutMethod(): boolean {
        return this.name === HTTP_METHOD_PUT;
    }
    isWriteMethod(): boolean {
        return this.isPatchMethod() || this.isPostMethod() || this.isPutMethod();
    }
    isGetMethod(): boolean {
        return this.name === HTTP_METHOD_GET;
    }
    isDeleteMethod(): boolean {
        return this.name === HTTP_METHOD_DELETE;
    }
    isProduceJson(): boolean {
        return SwaggerUtils.isType(this.produce, APPLICATION_JSON);
    }
    isProduceXml(): boolean {
        return SwaggerUtils.isType(this.produce, APPLICATION_XML);
    }
    isConsumeJson(): boolean {
        return SwaggerUtils.isType(this.consume, APPLICATION_JSON);
    }
    isConsumeXml(): boolean {
        return SwaggerUtils.isType(this.consume, APPLICATION_XML);
    }
    isConsumeFormUrlEncoded(): boolean {
        return SwaggerUtils.isType(this.consume, APPLICATION_FORM_URL_ENCODED);
    }
    isConsumeMultipartFormData(): boolean {
        return SwaggerUtils.isType(this.consume, MULTIPART_FORM_DATA);
    }
    getMapProduces(): { value: string }[] {
        return SwaggerUtils.getSelectMap(this.produces);
    }
    getMapConsumes(): { value: string }[] {
        return SwaggerUtils.getSelectMap(this.consumes);
    }
}

export class ParameterObject {
    name: string;
    'in': string;
    description: string;
    required: boolean;
    value: { selected: any };
    schema: ReferenceObject;
    collectionFormat: string;
    items: ItemsObject;
    type: string;
    constructor(_paramObj?: any) {
        this.items = new ItemsObject();
        this.value = { selected: '' };
        if (_paramObj) {
            Object.assign(this, _paramObj);
            if (_paramObj.schema) {
                this.schema = new ReferenceObject(_paramObj.schema);
            }
            if (_paramObj.items) {
                this.items = new ItemsObject(_paramObj.items);
            }
        }
    }
    isHeaderParam(): boolean {
        return this.in === HEADER_PARAM;
    }
    isPathParam(): boolean {
        return this.in === PATH_PARAM;
    }
    isQueryParam(): boolean {
        return this.in === QUERY_PARAM;
    }
    isBodyParam(): boolean {
        return this.in === BODY_PARAM;
    }
    isFormParam(): boolean {
        return this.in === FORM_PARAM;
    }
    isTypeEnum(): boolean {
        return this.items.enum && !(this.items.enum.length === 0);
    }
    isTypeFile(): boolean {
        return this.type === TYPE_FILE;
    }
    isTypeDate(): boolean {
        return this.type === TYPE_DATE;
    }
    getParameterType(): string {
        if (this.isBodyParam()) {
            if (SwaggerUtils.isTypeArray(this)) {
                return this.schema.items.entity;
            }
            return this.schema.entity;
        } else if (!SwaggerUtils.isTypeArray(this)) {
            return this.type;
        } else if (this.isTypeEnum() && this.items.enum.length > 0) {
            return 'Enum [' + this.items.enum.join(',') + ']';
        }
        return '[' + this.items.type + ']';
    }
    getEnumMap(): { value: string }[] {
        return this.items.enum.map((enumVal: string) => {
            return { value: enumVal, label: enumVal, selected: this.items && this.items.default === enumVal };
        });
    }
}

export class InfoObject {
    title: string;
    description: string;
    termsOfService: string;
    contact: ContactObject;
    license: LicenseObject;
    version: string;
    constructor(_info?: any) {
        this.contact = new ContactObject();
        this.license = new LicenseObject();
        if (_info) {
            Object.assign(this, _info);
            if (_info.contact) {
                this.contact = new ContactObject(_info.contact);
            }
            if (_info.license) {
                this.license = new LicenseObject(_info.license);
            }
        }
    }
}

export class ContactObject {
    name: string;
    url: string;
    email: string;
    constructor(_contact?: any) {
        if (_contact) {
            Object.assign(this, _contact);
        }
    }
}

export class LicenseObject {
    name: string;
    url: string;
    constructor(_license?: any) {
        if (_license) {
            Object.assign(this, _license);
        }
    }
}

export class PathsObject {
    name: string;
    path: PathItemObject;
    selected: boolean;
    constructor(name?: string, _pathItem?: any) {
        this.name = name;
        this.path = new PathItemObject();
        if (_pathItem) {
            this.path = new PathItemObject(name, _pathItem);
        }
    }
}

export class PathItemObject {
    $ref: string;
    path: string;
    parameters: (ParameterObject)[];
    operations: Array<OperationObject>;
    constructor(path?: string, _pathItemObj?: any) {
        this.path = path;
        this.operations = [];
        if (_pathItemObj) {
            Object.keys(_pathItemObj).forEach((method: string) => {
                this.operations.push(new OperationObject(path, method, _pathItemObj[method]));
            });
        }
    }
}

export class DefinitionsObject {
    name: string;
    schema: SchemaObject;
    constructor(name?: string, _defObj?: any) {
        this.name = name;
        this.schema = new SchemaObject();
        if (_defObj) {
            this.schema = new SchemaObject(_defObj);
        }
    }
    isRequired(fieldName: string): boolean {
        return this.schema.required.indexOf(fieldName) !== -1;
    }
}

export class ResponsesObject {
    code: string;
    response: ResponseObject;
    constructor(code: string, _respObj?: any) {
        this.code = code;
        if (_respObj) {
            this.response = new ResponseObject(_respObj[code]);
        }
    }
}

export class ResponsesDefinitionsObject {
    [index: string]: ResponseObject;
}

export class ResponseObject {
    description: string;
    schema: SchemaObject;
    headers: HeadersObject;
    examples: ExampleObject;
    items: ReferenceObject;
    constructor(_respObj?: any) {
        if (_respObj) {
            Object.assign(this, _respObj);
            if (_respObj.schema) {
                this.schema = new SchemaObject(_respObj.schema);
            }
            if (_respObj.examples) {
                this.examples = new ExampleObject(_respObj.examples);
            }
            if (_respObj.headers) {
                this.headers = new HeadersObject(_respObj.headers);
            }
            if (_respObj.items) {
                this.items = new ReferenceObject(_respObj.items);
            }
        }
    }
}

export class HeadersObject {
    [index: string]: ItemsObject;
    constructor(_headersObj?: any) {
        if (_headersObj) {
            Object.assign(this, _headersObj);
        }
    }
}

export class ExampleObject {
    [index: string]: any;
    constructor(_exampleObj?: any) {
        if (_exampleObj) {
            Object.assign(this, _exampleObj);
        }
    }
}

export class SecurityDefinitionsObject {
    [index: string]: SecuritySchemeObject;
}

export class SecuritySchemeObject {
    type: string;
    description: string;
    name: string;
    'in': string;
    flow: string;
    authorizationUrl: string;
    tokenUrl: string;
    scopes: ScopesObject;
}

export class ScopesObject {
    [index: string]: any;
}

export class SecurityRequirementObject {
    [index: string]: string[];
}

export class TagObject {
    name: string;
    description: string;
    externalDocs: ExternalDocumentationObject;
    constructor(_tagsObj?: any) {
        if (_tagsObj) {
            Object.assign(this, _tagsObj);
            if (_tagsObj.externalDocs) {
                this.externalDocs = new ExternalDocumentationObject(_tagsObj.externalDocs);
            }
        }
    }
}

export class ItemsObject {
    type: string;
    format: string;
    items: ItemsObject;
    collectionFormat: string;
    'default': any;
    maximum: number;
    exclusiveMaximum: boolean;
    minimum: number;
    exclusiveMinimum: boolean;
    maxLength: number;
    minLength: number;
    pattern: string;
    maxItems: number;
    minItems: number;
    uniqueItems: boolean;
    'enum': any[];
    multipleOf: number;
    constructor(_itemsObject?: any) {
        this.enum = [];
        if (_itemsObject) {
            Object.assign(this, _itemsObject);
            if (_itemsObject.items) {
                this.items = new ItemsObject(_itemsObject.items);
            }
        }
    }
}

export class ReferenceObject {
    $ref: string;
    entity: string;
    items: { $ref: string, entity: string };
    type: string;
    constructor(_refObj?: any) {
        if (_refObj) {
            Object.assign(this, _refObj);
            if (this.$ref) {
                this.entity = SwaggerUtils.extractEntityName(this.$ref);
            }
            if (this.items && this.items.$ref) {
                this.items.entity = SwaggerUtils.extractEntityName(this.items.$ref);
            }
        }
    }
}

export class ExternalDocumentationObject {
    [index: string]: any;
    description: string;
    url: string;
    constructor(_externDocObj?: any) {
        if (_externDocObj) {
            Object.assign(this, _externDocObj);
        }
    }
}

export class SchemaObject implements IJsonSchema {
    [index: string]: any;
    discriminator: string;
    readOnly: boolean;
    xml: XMLObject;
    externalDocs: ExternalDocumentationObject;
    example: any;
    items: ReferenceObject;
    $ref: string;
    entity: string;
    type: string;
    required: string[];
    properties: {
        [name: string]: IJsonSchema;
    };
    constructor(_schemaObj?: any) {
        this.required = [];
        this.properties = {};
        if (_schemaObj) {
            Object.assign(this, _schemaObj);
            if (_schemaObj.xml) {
                this.xml = new XMLObject(_schemaObj.xml);
            }
            if (_schemaObj.externalDocs) {
                this.externalDocs = new ExternalDocumentationObject(_schemaObj.externalDocs);
            }
            if (_schemaObj.items) {
                this.items = new ReferenceObject(_schemaObj.items);
            }
            if (_schemaObj.$ref) {
                this.entity = SwaggerUtils.extractEntityName(this.$ref);
            }
        }
    }
    isPropertyTypeArray(value: any): boolean {
        return SwaggerUtils.isArray(value.type);
    }
    getPropertyByName(name: string): string {
        if (this.properties[name]) {
            return this.properties[name].description;
        }
    }
}

export class XMLObject {
    [index: string]: any;
    name: string;
    namespace: string;
    prefix: string;
    attribute: boolean;
    wrapped: boolean;
    constructor(_xmlObject?: any) {
        if (_xmlObject) {
            Object.assign(this, _xmlObject);
        }
    }
}