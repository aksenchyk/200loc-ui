import { Plugin } from "./plugin";
import { Rule } from "./Rule";
import { EntryOption } from "./Condition";
import { Proxy } from "./Proxy";
import { HealthCheck, Health } from './HealthCheck';

export class Config {
    id?: string;
    name?: string = '';
    description?: string = '';
    entry?: string = '/';
    methods?: Array<string> = ["GET", "POST"];
    plugins?: Array<Plugin> = [];
    conditions?: Array<EntryOption> = [];
    proxy?: Proxy = new Proxy({
        target: 'http://localhost:8080/',
        active: true,
        prependPath: true,
        secure: false,
        ignorePath: false,
        changeOrigin: false,
        toProxy: false,
        xfwd: false,
        auth: '',
    });
    rules?: Array<Rule> = [new Rule({ path: '/*', plugins: [], policy:`[]
                    
                    
                    
                         
    ` })];
    health?: Health = new Health({ healthCheck: [] });
    constructor(data?) {
        if (data) {
            Object.assign(this, data);
        }
    }
}
