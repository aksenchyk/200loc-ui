/* tslint:disable */
export interface IPlugin {
    name?: string;
    description?: string;
    displayName?: string;
    dependenciesTemplate?: any;
    settingsTemplate?: any;
    order?: number;
    active?: boolean;
    valid?: boolean;
    dependencies?: Array<any>;
    settings?: Array<any>;
}

export class Plugin {
    name: string;
    displayName: string;
    description: string;
    dependenciesTemplate: any;
    settingsTemplate: any = {};
    order?: number;
    active?: boolean = false;
    valid?: boolean = false;
    dependencies?: any = {};
    settings?: any = {};

    constructor(instance?: IPlugin) {
        Object.assign(this, instance);
    }
}
