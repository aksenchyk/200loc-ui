/* tslint:disable: no-switch-case-fall-through */
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';

import { User } from '../models';
import { HealthActions } from '../actions';

export interface ClusterHealthState extends Array<any> {
};

const initialState: ClusterHealthState = [];

export function HealthReducer(state = initialState, action: Action): ClusterHealthState {
    switch (action.type) {
        case HealthActions.SET_HEALTH: {
            return action.payload
                ? [...action.payload]
                : [];
        }
        default: {
            return state;
        }
    }
}

export function getCurrentClusterHealth() {
    return (state$: Observable<ClusterHealthState>) => state$;       
}
