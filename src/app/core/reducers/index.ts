import { Observable } from 'rxjs/Observable';
import { compose } from '@ngrx/core/compose';
import { combineReducers } from '@ngrx/store';

import * as master from './master';
import * as defaults from './defaults';
import * as validation from './masterValidation';
import * as config from './config';

import { masterReducer, EntryCreationMasterState } from './master';
import { defaultsReducer, DefaultAppState, isLoaded } from './defaults';
import { EntryValidityState, masterValidityReducer } from './masterValidation';
import { ConfigState, configReducer } from './config';

import { UserAuthenticationState, authenticationReducer, getAuthenticated } from './authentication.reducer';
import { HealthReducer, ClusterHealthState, getCurrentClusterHealth } from './health.reducer';
import { StatsReducer, EntryStatsState, getCurrentEntryStats } from './stats.reducer';
import { NodesReducer, NodesState, getNodes } from './nodes.reducer';

export interface AppState {
    master: EntryCreationMasterState;
    defaults: DefaultAppState;
    validation: ValidityState;
    config: ConfigState,
    authentication: UserAuthenticationState,
    health: ClusterHealthState,
    stats: EntryStatsState,
    nodes: NodesState
}

export default combineReducers({
    master: masterReducer,
    defaults: defaultsReducer,
    validation: masterValidityReducer,
    config: configReducer,
    authentication: authenticationReducer,
    health: HealthReducer,
    stats: StatsReducer,
    nodes: NodesReducer
});

/**
 * Get current master configuration as form value and 
 */
export function getMasterState() {
    return (state$: Observable<AppState>) => {
        return state$
            .select(s => s.master)
    };
}
export function getDefaultsState() {
    return (state$: Observable<AppState>) => {
        return state$
            .select(s => s.defaults)
    };
}

export function getValidationState() {
    return (state$: Observable<AppState>) => {
        return state$
            .select(s => s.validation)
    };
}

export function getAuthenticationState() {
    return (state$: Observable<AppState>) => {
        return state$
            .select(s => s.authentication)
    };
}

export function getHealthState() {
    return (state$: Observable<AppState>) => {
        return state$
            .select(s => s.health)
    };
}

export function getStatsState() {
    return (state$: Observable<AppState>) => {
        return state$
            .select(s => s.stats)
    };
}

export function getNodesState() {
    return (state$: Observable<AppState>) => {
        return state$
            .select(s => s.nodes)
    };
}
/**
 * Get state of current entry config being edited in master (may be newly created of taken from the backend)
 * This config goes as initial value for entry configuration master
 */
export function getConfigState() {
    return (state$: Observable<AppState>) => {
        return state$
            .select(s => s.config)
    };
}

/**
 * Get all available plugins installed in system
 */
export function getPlugins() {
    return compose(defaults.getAvailablePlugins(), getDefaultsState());
}


/**
 * Get all available services installed in system
 */
export function getServices() {
    return compose(defaults.getAvailableServices(), getDefaultsState());
}


export function getMasterConfigPlugins() {
    return compose(master.getMasterPlugins(), getMasterState());
}


export function getLoaded() {
    return compose(defaults.isLoaded(), getDefaultsState());
}

export function getHealth() {
    return compose(getCurrentClusterHealth(), getHealthState());
}

export function getStats() {
    return compose(getCurrentEntryStats(), getStatsState());
}

export function getWorkingNodes() {
    return compose(getNodes(), getNodesState());
}

/**
 * Get observable of user's authentication state
 * 
 * @returns Observable<boolean> 
 */
export function isUserLoggedIn() {
    return compose(getAuthenticated(), getAuthenticationState())
}