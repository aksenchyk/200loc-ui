/* tslint:disable: no-switch-case-fall-through */
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { NodesActions } from '../actions';

export interface NodesState extends Array<any> { };

const initialState: NodesState = [];

export function NodesReducer(state = initialState, action: Action): NodesState {
    switch (action.type) {
        case NodesActions.SET_NODES: {
            return action.payload && Array.isArray(action.payload)
                ? [...action.payload]
                : [];
        }
        default: {
            return state;
        }
    }
}

export function getNodes() {
    return (state$: Observable<NodesState>) => state$;
}
