/* tslint:disable: no-switch-case-fall-through */
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';

import { User } from '../models';
import { StatsActions } from '../actions';

const getInitial = () => ({
    statuses: {},
    average: 0,
    load: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    id: 'initial'
})

export interface EntryStatsState {
    statuses: any;
    average: number;
    load: Array<number>;
    id: string;
};

const initialState: EntryStatsState = getInitial();

export function StatsReducer(state = initialState, action: Action): EntryStatsState {
    switch (action.type) {
        case StatsActions.SET_STATS: {

            const load: any[] = [...action.payload.load];
            const average: number = action.payload.average;
            const statuses: any = action.payload.statuses;
            const newState = Object.assign({}, state, { statuses: statuses, average: average, load: load, id: action.payload.id });
            console.log('=======================NEW STATS SATE===========', newState)
            return newState;

        }
        default: {
            return state;
        }
    }
}

export function getCurrentEntryStats() {
    return (state$: Observable<EntryStatsState>) => state$;
}
