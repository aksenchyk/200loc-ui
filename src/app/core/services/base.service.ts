/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Headers, Request, ResponseContentType } from '@angular/http';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { JSONSearchParams } from './search.params';
import { ErrorHandler } from './error.service';
import { LoopBackAuth } from './auth.service';
import { CoreConfig } from '../core.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

/**
 * @module BaseRestApi
 **/
@Injectable()
export abstract class BaseRestApi {

  protected path: string;

  constructor(
    @Inject(Http) protected http: Http,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) { }

  /**
   * Process request
   * @param string  method      Request method (GET, POST, PUT)
   * @param string  url         Request url (my-host/my-url/:id)
   * @param any     routeParams Values of url parameters
   * @param any     urlParams   Parameters for building url (filter and other)
   * @param any     postBody    Request postBody
   * @param boolean isio        Request socket connection (When IO is enabled)
   */
  public request(
    method: string,
    url: string,
    routeParams: any = {},
    urlParams: any = {},
    postBody: any = null,
    headers: Headers = new Headers({ "Content-Type": "application/json" }),
    responseContentType: ResponseContentType = ResponseContentType.Json
  ) {

    if (this.auth.user && this.auth.user.accessToken) {
      headers.append(
        'Authorization', this.auth.user.accessToken
      );
    }

    let requestUrl = url;
    let key: string;
    for (key in routeParams) {
      requestUrl = requestUrl.replace(
        new RegExp(":" + key + "(\/|$)", "g"),
        routeParams[key] + "$1"
      );
    }

    // Body fix for built in remote methods using "data", "options" or "credentials
    // that are the actual body, Custom remote method properties are different and need
    // to be wrapped into a body object
    let body: any;
    if (
      typeof postBody === 'object' &&
      (postBody.data || postBody.credentials || postBody.options) &&
      Object.keys(postBody).length === 1
    ) {
      body = postBody.data ? postBody.data :
        postBody.options ? postBody.options :
          postBody.credentials;
    } else {
      body = postBody;
    }
    this.searchParams.setJSON(urlParams);
    let request: Request = new Request({
      headers: headers,
      method: method,
      url: requestUrl,
      search: this.searchParams.getURLSearchParams(),
      body: body ? JSON.stringify(body) : undefined,
      responseType: responseContentType
    });
    return this.http.request(request)
      .map((res: any) => {
        if (responseContentType !== ResponseContentType.Blob) {
          return res.text() != "" ? res.json() : {};
        } else {
          return res.blob();
        }
      })
      .catch(this.errorHandler.handleError.bind(this.errorHandler));
  }
}
