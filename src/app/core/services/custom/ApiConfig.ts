/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response, Headers, ResponseContentType } from '@angular/http';
import { BaseRestApi } from '../base.service';
import { CoreConfig } from '../../core.config';
import { LoopBackAuth } from '../auth.service';
import { LoopBackFilter } from '../../models/BaseModels';
import { JSONSearchParams } from '../search.params';
import { ErrorHandler } from '../error.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { ApiConfig } from '../../models/ApiConfig';

// Making Sure EventSource Type is available to avoid compilation issues.
declare var EventSource: any;

/**
 * Api services for the `ApiConfig` model.
 */
@Injectable()
export class ApiConfigApi extends BaseRestApi {

    constructor(
        @Inject(Http) http: Http,
        @Inject(LoopBackAuth) protected auth: LoopBackAuth,
        @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
        @Optional() @Inject(ErrorHandler) errorHandler: ErrorHandler
    ) {
        super(http, auth, searchParams, errorHandler);
    }

    /**
     * Create a new instance of the model and persist it into the data source.
     *
     * @param object data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `ApiConfig` object.)
     * </em>
     */
    public create(data: any = undefined) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes";
        let routeParams: any = {};
        let postBody: any = {
            data: data
        };
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result.map((instance: ApiConfig) => new ApiConfig(instance));
    }

    /**
      * Create a new instance of the model and persist it into the data source.
      *
      * @param object data Request data.
      *
      * This method expects a subset of model properties as request parameters.
      *
      * @returns object An empty reference that will be
      *   populated with the actual data once the response is returned
      *   from the server.
      *
      * <em>
      * (The remote method definition does not provide any description.
      * This usually means the response is a `Cart` object.)
      * </em>
      */
    public updateOrCreate(data: any = {}) {
        let method: string = "PATCH";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes";
        let routeParams: any = {};
        let postBody: any = {
            data: data
        };
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result.map((instance: ApiConfig) => new ApiConfig(instance));
    }

    /**
     * Find a model instance by {{id}} from the data source.
     *
     * @param any id Model id
     *
     * @param object filter Filter defining fields and include
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `ApiConfig` object.)
     * </em>
     */
    public findById(id: any, filter: LoopBackFilter = undefined) {
        let method: string = "GET";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes/:id";
        let routeParams: any = {
            id: id
        };
        let postBody: any = {};
        let urlParams: any = {};
        if (filter) urlParams.filter = filter;
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result.map((instance: ApiConfig) => new ApiConfig(instance));
    }

    /**
     * Find all instances of the model matched by filter from the data source.
     *
     * @param object filter Filter defining fields, where, include, order, offset, and limit
     *
     * @returns object[] An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `ApiConfig` object.)
     * </em>
     */
    public find(filter: LoopBackFilter = undefined) {
        let method: string = "GET";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes";
        let routeParams: any = {};
        let postBody: any = {};
        let urlParams: any = {};
        if (filter) urlParams.filter = filter;
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result.map((instances: Array<ApiConfig>) => {
            return instances.map((instance: ApiConfig) => new ApiConfig(instance))
        });
    }

    /**
     * Delete a model instance by {{id}} from the data source.
     *
     * @param any id Model id
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `ApiConfig` object.)
     * </em>
     */
    public deleteById(id: any) {
        let method: string = "DELETE";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes/:id";
        let routeParams: any = {
            id: id
        };
        let postBody: any = {};
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }

    /**
     * Count instances of the model matched by where from the data source.
     *
     * @param object where Criteria to match model instances
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `count` – `{number}` - 
     */
    public count(where: any = undefined) {
        let method: string = "GET";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes/count";
        let routeParams: any = {};
        let postBody: any = {};
        let urlParams: any = {};
        if (where) urlParams.where = where;
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }

    public testApiConfig(methodToTest, requestPath, config, headers, params, body?) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes/test";
        let routeParams: any = {};
        let postBody: any = {
            data: { method: methodToTest, path: requestPath, config, headers, params, body }
        };
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }


    public healthcheck(health) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes/healthcheck";
        let routeParams: any = {};
        let postBody: any = {
            data: health
        };
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }

    public stats(id: any) {
        let method: string = "GET";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes/stats/:id";
        let routeParams: any = {
            id: id
        };
        let postBody: any = {};
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result.map((instance: ApiConfig) => new ApiConfig(instance));
    }

    public clone(id: any) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/routes/clone/" + id;
        let routeParams: any = {};
        let postBody: any = {
        };
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }

    public export() {
        let method: string = "GET";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/export";
        let routeParams: any = {};
        let postBody: any = {};
        let urlParams: any = {};
        let headers = new Headers();
        headers.append('Content-Type', 'application/zip');
        headers.append("Accept", "application/zip");
        let result = this.request(method, url, routeParams, urlParams, postBody, headers, ResponseContentType.Blob)
        return result;
    }

    public import(data) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/import";
        let routeParams: any = {};
        let postBody: any = {
            data: data
        };
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }

}
