/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { CoreConfig } from '../../core.config';
import { LoopBackFilter } from '../../models/BaseModels';
import { LoopBackAuth } from '../auth.service';
import { BaseRestApi } from '../base.service';
import { JSONSearchParams } from '../search.params';
import { ErrorHandler } from '../error.service';
import { User } from '../../models';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';

// Making Sure EventSource Type is available to avoid compilation issues.
declare var EventSource: any;

/**
 * Api services for the `User` model.
 */
@Injectable()
export class UserApi extends BaseRestApi {

    constructor(
        @Inject(Http) http: Http,
        @Inject(LoopBackAuth) protected auth: LoopBackAuth,
        @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
        @Optional() @Inject(ErrorHandler) errorHandler: ErrorHandler
    ) {
        super(http, auth, searchParams, errorHandler);
    }
   
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param object data Request data.
     *
     *  - `userId` – `{string}` - 
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `User` object.)
     * </em>
     */
    public deleteUser(userId: any = undefined) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + CoreConfig.getApiVersion() + "/private" +
            "/users/deleteuserandprofile";
        let routeParams: any = {};
        let postBody: any = {
            userId: userId
        };
        let urlParams: any = {};
        if (userId) urlParams.userId = userId;
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }

    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param object data Request data.
     *
     *  - `userId` – `{string}` - 
     *
     *  - `data` – `{object}` - 
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `User` object.)
     * </em>
     */
    public updatePassword(data: any = undefined) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + CoreConfig.getApiVersion() + "/private" +
            "/users/updatepassword";
        let routeParams: any = {};
        let postBody: any = data;
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }

    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param object data Request data.
     *
     *  - `userId` – `{string}` - 
     *
     *  - `data` – `{object}` - 
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `User` object.)
     * </em>
     */
    public updateAccount(data: any = undefined) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + CoreConfig.getApiVersion() + "/private" +
            "/users/updateaccount";
        let routeParams: any = {};
        let postBody: any = data;
        let result = this.request(method, url, routeParams, {}, postBody);
        return result;
    }


    /**
     * Login a user with username/email and password.
     *
     * @param string include Related objects to include in the response. See the description of return value for more details.
     *   Default value: `user`.
     *
     *  - `rememberMe` - `boolean` - Whether the authentication credentials
     *     should be remembered in localStorage across app/browser restarts.
     *     Default: `true`.
     *
     * @param object data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * The response body contains properties of the AccessToken created on login.
     * Depending on the value of `include` parameter, the body may contain additional properties:
     * 
     *   - `user` - `U+007BUserU+007D` - Data of the currently logged in user. (`include=user`)
     * 
     *
     */

    public login(credentials: { username: string, password: string, remember: boolean }, include: any = 'user') {
        let method: string = "POST";
        // let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
        //     "/login";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/users/login";

        let routeParams: any = {};
        let postBody: any = {
            data: credentials
        };
        let urlParams: any = {};
        if (include) urlParams.include = include;
        let result = this.request(method, url, routeParams, urlParams, postBody)
            .share();
        result.subscribe(
            (response: { id: string, userId: string, user: any }) => {
                if (credentials.remember)
                    this.auth.persist({ accessToken: response.id, username: response.user.username });
            },
            (err) => { console.error(err) }
        );
        return result;
    }

    /**
     * Change default user username/password
     *
     * @param string include Related objects to include in the response. See the description of return value for more details.
     *   Default value: `user`.
     *
     *  - `rememberMe` - `boolean` - Whether the authentication credentials
     *     should be remembered in localStorage across app/browser restarts.
     *     Default: `true`.
     *
     * @param object data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * The response body contains properties of the AccessToken created on login.
     * Depending on the value of `include` parameter, the body may contain additional properties:
     * 
     *   - `user` - `U+007BUserU+007D` - Data of the currently logged in user. (`include=user`)
     * 
     *
     */
    public change(credentials: any) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/users/change";

        let routeParams: any = {};
        let postBody: any = {
            data: credentials
        };
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }


    /**
     * 
     * Create new user with username/email and password.
     *
     *
     * @param object data Request data.
     *
     *  - `data` – `{object}` - 
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `User` object.)
     * </em>
     */
    public signup(data: any = undefined) {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/users/signup";
        let routeParams: any = {};
        let postBody: any = {
            data: data
        };
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }


    /**
     * Logout a user with access token.
     *
     * @param object data Request data.
     *
     *  - `access_token` – `{string}` - Do not supply this argument, it is automatically extracted from request headers.
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    public logout() {
        let method: string = "POST";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/users/logout";
        let routeParams: any = {};
        let postBody: any = {};
        let urlParams: any = {};
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }
}