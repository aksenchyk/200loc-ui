/* tslint:disable */
export * from './ApiConfig';
export * from './UserApi';
export * from './PluginsApi.service';
export * from './ServiceApi.service';
export * from './HealthChecksApi.service';

import { ApiConfigApi } from './ApiConfig';
import { UserApi } from './UserApi';
import { PluginApi } from './PluginsApi.service';
import { ServiceApi } from './ServiceApi.service';
import { HealthcheckApi } from './HealthChecksApi.service';

export const CUSTOM_SERVICES = [
    ApiConfigApi,
    UserApi,
    PluginApi,
    ServiceApi,
    HealthcheckApi
]