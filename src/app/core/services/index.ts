/* tslint:disable */
export * from './auth.service';
export * from './error.service';
export * from './search.params';
export * from './base.service';
export * from './stats.service';
export * from './custom';

import { LoopBackAuth } from './auth.service';
import { ErrorHandler } from './error.service';
import { JSONSearchParams } from './search.params';
import { StatsService } from './stats.service';
import { CUSTOM_SERVICES } from './custom';

export const CORE_SERVICES = [
    LoopBackAuth,
    StatsService,
    ErrorHandler,
    JSONSearchParams,   
    ...CUSTOM_SERVICES
]
