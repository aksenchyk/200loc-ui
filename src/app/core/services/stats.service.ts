/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BaseRestApi } from './base.service';
import { CoreConfig } from './../core.config';
import { LoopBackAuth } from './auth.service';
import { LoopBackFilter } from './../models/BaseModels';
import { ServiceStatus } from './../models';
import { JSONSearchParams } from './search.params';
import { ErrorHandler } from './error.service';
import { Subject, Observable } from 'rxjs';
import { Service } from './../models/Service';

// Making Sure EventSource Type is available to avoid compilation issues.
declare var EventSource: any;

/**
 * Api services for the `Service` model.
 */
@Injectable()
export class StatsService extends BaseRestApi {

    constructor(
        @Inject(Http) http: Http,
        @Inject(LoopBackAuth) protected auth: LoopBackAuth,
        @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
        @Optional() @Inject(ErrorHandler) errorHandler: ErrorHandler
    ) {
        super(http, auth, searchParams, errorHandler);
    }

    public stats(action, payload?) {
        let method: string = "GET";
        let url: string = CoreConfig.getPath() + "/" + CoreConfig.getApiVersion() +
            "/stats";
        let routeParams: any = {};
        let postBody: any = {};
        let urlParams: any = {};
        urlParams = {
            action: action,
            ...payload
        };
        let result = this.request(method, url, routeParams, urlParams, postBody);
        return result;
    }

}
