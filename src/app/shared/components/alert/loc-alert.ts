import {
    Component, OnInit, Input,
    HostBinding, Attribute, Optional,
    Output, EventEmitter, Renderer, ElementRef
} from '@angular/core';

@Component({
    selector: 'loc-alert',
    template: `
    
    <div *ngIf='error || warning || success || showContent' class='__alert __alert-{{getClass()}}'    
        [class.fixed]='fixed'     
        [class.small]='small'>
        <a (click)='close.emit()'><i class="icon-close-round"></i></a>
        <span *ngIf='!showContent'>{{error || warning || success}}</span>
        <ng-content></ng-content>
    </div>
    
    `,
    host: {
        '[style.display]': "'block'",
        '[style.position]': "fixed ? 'absolute':'static'",
        '[style.left]': "'0'",
        '[style.right]': "'0'",
        '[style.top]': "'0'",
        '[style.z-index]': "'100300'"
    },
    styleUrls: ['./loc-alert.scss']
})
export class LocAlertComponent {
    @Input() error: string;
    @Input() success: string;
    @Input() warning: string;
    @Input('css') class: string;
    @Input() showContent: string;

    @Output() close: EventEmitter<any> = new EventEmitter();

    public fixed: boolean = false;
    public fixedBottom: boolean = false;
    public small: boolean = false;   

    constructor(
        @Optional() @Attribute('fixedTop') fixedTop: string,      
        @Optional() @Attribute('fixedBottom') fixedBottom: string,       
        private _rendeder: Renderer,
        private _element: ElementRef
    ) {
        if (typeof fixedTop === 'string') {
            this.fixed = true;
        }
        if (typeof fixedBottom === 'string') {
            this.fixedBottom = true;
        }     
      
    }
    getClass() {
        if (!!this.class) return this.class;
        if (!!this.error) return 'error';
        if (!!this.success) return 'success';
        if (!!this.warning) return 'warning';
    }
    ngOnInit() {
        this._checkElementSize();
        this._rendeder.listenGlobal('window', 'resize', (evt: any) => {
            this._checkElementSize();
        });
    }

    private _checkElementSize() {
        const rect = this._element.nativeElement.getBoundingClientRect();
        if (rect.width < 320) {
            this.small = true;
        } else {
            this.small = false;
        }
    }
}