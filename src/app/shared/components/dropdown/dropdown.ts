import {
    Component, OnInit, AfterViewInit, Output,
    Input, EventEmitter, OnDestroy, Host, Optional,
    trigger, state, style, animate, transition
} from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

@Component({
    selector: 'loc-dropdown',
    templateUrl: "./dropdown.html",
    styleUrls: ['./dropdown.scss'],
    animations: [
        trigger('detailsCollapse', [
            state('expanded', style({
                height: '*',
                visibility: 'visible',
                opacity: '1'
            })),
            state('collapsed', style({
                height: '0px',
                visibility: 'hidden',
                opacity: '0'
            })),
            transition('expanded => collapsed', animate('250ms cubic-bezier(0.4,0.0,0.2,1)')),
            transition('collapsed => expanded', animate('250ms cubic-bezier(0.4,0.0,0.2,1)'))
        ]),
        trigger('headerCollapse', [
            state('expanded', style({
                background: '#fafafa',
                borderColor: 'red'
            })),
            state('collapsed', style({
                background: '#fff'
            })),
            transition('* => *', animate('250ms linear'))
        ]),
        trigger('iconState', [
            state('expanded', style({ transform: 'rotate(0deg)' })),
            state('collapsed', style({ transform: 'rotate(180deg)' })),
            transition('expanded => collapsed', animate('.4s ease-in')),
            transition('collapsed => expanded', animate('.4s ease-out')),
        ])
    ]
})
export class DropdownComponent {
    @Output()
    change: EventEmitter<any> = new EventEmitter();
    @Output()
    remove: EventEmitter<any> = new EventEmitter();
    @Input()
    title: string;

    state: string = 'collapsed';

    optionForm: FormGroup;
    private _condition;

    constructor() {
    }

    toggle() {
        if (this.state == 'collapsed') {
            this.state = 'expanded';
            return;
        }
        if (this.state == 'expanded') {
            this.state = 'collapsed';
        }
    }
}

