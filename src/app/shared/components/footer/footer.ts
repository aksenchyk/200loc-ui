import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'loc-footer',
    templateUrl: './footer.html',
    styles:[
        `
        :host {
            display: block;
            width: 100%;
        }
        
        `
    ]
})
export class FooterComponent implements OnInit {
    constructor() { }
    seo = {
        title: '200Loc',
        version: '0.7.0-alpha.1'
    };
    ngOnInit() { }
}
