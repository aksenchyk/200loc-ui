import { LoaderComponent } from './loader/loader';
import { HEADER_COMPONENTS } from './header';
import { FooterComponent } from './footer/footer';
import { AceEditorComponent } from './ace-editor/aceEditorComponent';
import { POPOVER_DIRECTIVES } from './popover';
import { SwitchComponent } from './switch';
import { DropdownComponent } from './dropdown';
import { DynamicForm } from './dynamic-form';
import { SIDEBAR_COMPONENTS } from './sidebar';
import { LocPagerComponent } from './pager';
import { StatisticComponent } from './statistic';
import { InlineEditComponent } from './inline-edit/inline-edit';
import { InputText } from './input-text';
import { InputTextArea } from './input-text-area';
import { SelectInput } from './input-select';
import { InputAce } from './input-ace';
import { LocAlertComponent } from './alert';
import { NodeListComponent } from './nodes-list';

export * from './loader/loader';
export * from './header';
export * from './footer/footer';
export * from './ace-editor/aceEditorComponent';
export * from './switch';
export * from './dynamic-form';
export * from './pager';
export * from './statistic';
export * from './inline-edit/inline-edit';
export * from './input-text';
export * from './nodes-list';

export var SHARED_COMPONENTS = [
    LoaderComponent,
    FooterComponent,
    AceEditorComponent,
    SwitchComponent,
    DynamicForm,
    ...POPOVER_DIRECTIVES,
    ...SIDEBAR_COMPONENTS,
    ...HEADER_COMPONENTS,
    LocPagerComponent,
    StatisticComponent,
    InlineEditComponent,
    InputText, InputTextArea,
    LocAlertComponent,
    SelectInput,
    InputAce, 
    NodeListComponent,
    DropdownComponent
]
