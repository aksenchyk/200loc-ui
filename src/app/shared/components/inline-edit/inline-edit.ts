import {
    Component, Output,
    Provider, forwardRef,
    EventEmitter, ElementRef, ViewChild,
    Renderer, OnInit
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { InputText } from '../input-text';
@Component({
    selector: 'inline-edit',
    styleUrls: ['./inline-edit.scss'],
    templateUrl: './inline-edit.tmpl.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InlineEditComponent),
            multi: true
        }
    ]
})
export class InlineEditComponent implements ControlValueAccessor {

    // inline edit form control
    // @ViewChild('inlineEditControl') inlineEditControl;
    @ViewChild(InputText) inlineEditControl;
    @Output() public onSave: EventEmitter<any> = new EventEmitter();

    private __value: string = '';
    private __preValue: string = '';
    editing: boolean = false;

    get value(): any { return this.__value; };

    set value(v: any) {
        if (v !== this.__value) {
            this.__value = v;
            this.onChange(v);
        }
    }

    constructor(element: ElementRef, private _renderer: Renderer) { }


    // Method to display the inline edit form and hide the <a> element
    edit(value) {
        this.__preValue = value;  // Store original value in case the form is cancelled
        this.editing = true;

        // Automatically focus input
        // setTimeout(_ => this._renderer.invokeElementMethod(this.inlineEditControl.nativeElement, 'focus', []));
        setTimeout(_ => this.inlineEditControl.focus());
       
    }

    // Method to display the editable value as text and emit save event to host
    onSubmit(value) {
        this.onSave.emit(value);
        this.editing = false;
    }

    // Method to reset the editable value
    cancel(value: any) {
        this.__value = this.__preValue;
        this.editing = false;
    }

    /**
    * ControlValueAccessor
    */
    onChange = (_: any) => {
    };
    onTouched = () => {
    };
    writeValue(value) {
        if (value)
            this.__value = value;
    }
    registerOnChange(fn): void {
        this.onChange = fn;
    }
    registerOnTouched(fn): void {
        this.onTouched = fn;
    }
}