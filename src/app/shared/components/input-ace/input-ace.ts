import {
    Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation,
    ViewChild, forwardRef, HostBinding, Renderer
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, NgModel } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
    selector: 'loc-input-ace',
    templateUrl: './input-ace.html',   
    styleUrls: ['./input-ace.scss'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => InputAce),
        multi: true
    }],
    host: {
        '[style.outline]': "'none'",      
        '[style.display]': "'block'"
    },
})

export class InputAce implements ControlValueAccessor {
    @Input() name: string;
    @Input() type: string = 'text';
    @Input() label: string;
    @Input() value: any;
    @Input() prefix: string;
    @Input() suffix: string;
    @Input() hasError: string;
    @ViewChild('inputControl') inputControl;
    @Input('isDisabled') disabled: boolean = false;
    @Input('always-float-label') alwaysFloatLabel: boolean = true;
    @Input('no-label-float') noLabelFloat: boolean;
    @Input('image') imagable: boolean;
    @Output('focus') focusEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output('blur') blurEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output('change') change: EventEmitter<any> = new EventEmitter<any>();
    @Input() aceOptions = { maxLines: 1000, printMargin: false };
    constructor(private _renderer: Renderer) {
    }

    ngAfterViewInit() {
    }

    private onTouchedCallback: () => void = () => { };
    writeValue(value: any) {
        this.value = value;
    }
    registerOnChange(fn: any) {
        this.change.subscribe(fn)
    }
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
    setDisabledState(isDisabled: boolean) {
        this.disabled = isDisabled;
    }
    hasValue() {
        return !!this.value && this.value != 0;
    }
    focus() {
        if (this.inputControl)
            this._renderer.invokeElementMethod(this.inputControl.nativeElement, 'focus', []);
    }
}
