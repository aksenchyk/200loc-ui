import {
    Component,
    Input,
    Output,
    EventEmitter,
    ElementRef, ViewChild, forwardRef, Renderer, QueryList, ViewChildren, HostListener
} from '@angular/core';
import { getDOM, DomAdapter } from '@angular/platform-browser/src/dom/dom_adapter';
import {
    NgClass,
    NgStyle
} from '@angular/common';

import { NG_VALUE_ACCESSOR, ControlValueAccessor, NgModel } from '@angular/forms';
import { Observable } from 'rxjs';

import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { SelectItem } from './select-item';
// import {
//     stripTags
// } from '../../pipes';

export function escapeRegexp(queryToEscape: string): string {
    return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
}


function stripTags(input: string): string {
    let tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
    let commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '').replace(tags, '');
}

@Component({
    selector: 'loc-select',
    templateUrl: './input-select.html',
    styleUrls: ['./input-select.scss'],
    host: {
        '[style.outline]': "'none'",
        // '[style.padding-top]': "'15px'",
        '[style.display]': "'block'"
    },
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => SelectInput),
        multi: true
    }],
})
export class SelectInput implements ControlValueAccessor {
    @ViewChild('selectInput') input: ElementRef;
    @ViewChild('choicesContent') choicesContent: ElementRef;
    @ViewChildren('choices') choices: QueryList<ElementRef>;

    @Input() public allowClear: boolean = false;
    @Input() public placeholder: string = 'Select';
    @Input() public idField: string = 'id';
    @Input() public textField: string = 'text';
    @Input() public multiple: boolean = false;
    @Input() public max: number = 0;
    @Input() public simple: boolean;
    @Input('image') imagable: boolean;
    @Input() label: string;
    @Input() hasError: string;

    public activeValue;

    @Output('change') change: EventEmitter<any> = new EventEmitter<any>();


    @Input()
    public set items(value: Array<any>) {
        if (!value) {
            this._items = this.itemObjects = [];
        } else {
            this._items = value.filter((item: any) => {
                if ((typeof item === 'string') || (typeof item === 'object' && item.text) || (this.textField && typeof item === 'object' && item[this.textField])) {
                    return item;
                }
            });
            this.itemObjects = this._items.map((item: any) => new SelectItem(item, this.textField, this.idField));
            this.options = this.itemObjects;
        }
        if (this.activeValue) {
            this._applyActiveValue(this.activeValue);
        }
    }
    public get items() {
        return this._items;
    }

    @Input()
    public set disabled(value: boolean) {
        this._disabled = value;
        if (this._disabled === true) {
            this.hideOptions();
        }
    }
    public get disabled(): boolean {
        return this._disabled;
    }

    @Input()
    public set active(selectedItem: SelectItem) {
        this._active = selectedItem;
    }
    public get active(): SelectItem {
        return this._active;
    }

    @Output() public data: EventEmitter<any> = new EventEmitter();
    @Output() public selected: EventEmitter<any> = new EventEmitter();
    @Output() public removed: EventEmitter<any> = new EventEmitter();
    @Output() public typed: EventEmitter<any> = new EventEmitter();
    @Output() public opened: EventEmitter<any> = new EventEmitter();

    public options: Array<SelectItem> = [];
    public itemObjects: Array<SelectItem> = [];
    public activeOption: SelectItem;

    public set optionsOpened(value: boolean) {
        this._optionsOpened = value;
        this.opened.emit(value);
    }

    public get optionsOpened(): boolean {
        return this._optionsOpened;
    }

    private __stringToBoolean(string) {
        switch (string.toLowerCase().trim()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(string);
        }
    }

    private inputMode: boolean = false;
    private _optionsOpened: boolean = false;
    private inputValue: string = '';
    private _items: Array<any> = [];
    private _disabled: boolean = false;
    private _active: SelectItem;

    constructor(private element: ElementRef, private sanitizer: DomSanitizer) { }

    sanitize(html: string): SafeHtml {
        return this.sanitizer.bypassSecurityTrustHtml(html);
    }

    close(): void {
        this.inputMode = false;
        this.optionsOpened = false;
    }

    protected selectActive(item: SelectItem): void {
        this.activeOption = item;
    }

    protected isActive(item: SelectItem): boolean {
        return this.activeOption.text === item.text;
    }

    open(): void {
        this.options = this.itemObjects;
        this.optionsOpened = true;
    }

    hideOptions(): void {
        this.inputMode = false;
        this.optionsOpened = false;
    }

    selectActiveMatch(): void {
        this.selectMatch(this.activeOption);
    }

    selectMatch(item: SelectItem, emit = false, e: Event = void 0): void {
        if (e) {
            e.stopPropagation();
            e.preventDefault();
        }
        if (this.options.length <= 0) {
            return;
        }
        this.activeOption = item;
        if (emit) {
            this.change.emit(item.value);
        }
        this.hideOptions();
    }

    onTouchedCallback: () => void = () => { };

    writeValue(value: any) {
        if (!value) {
            return;
        }
        if (value === 'true' || value === 'false') {
            value = this.__stringToBoolean(value)
        }
        this.activeValue = value;
        if (!this.items || !(this.items.length > 0)) return;
        this._applyActiveValue(value);
    }

    private _applyActiveValue(value) {
        if (!this.items || !Array.isArray(this.items)) {
            return;
        }
        let key = typeof value === 'object'
            ? value[this.idField]
            : value;
        this.activeValue = this.items.find((i) => {
            if (typeof i === 'object') {
                return i[this.idField] == key;
            }
            return i === key;
        });
        if (this.activeValue) {
            this.selectMatch(new SelectItem(this.activeValue, this.textField, this.idField));
        }
    }

    registerOnChange(fn: any) {
        this.change.subscribe(fn)
    }
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
}

