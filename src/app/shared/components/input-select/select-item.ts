export class SelectItem {
    public value: string;
    public text: string;

    constructor(source: any, textField: string, idField: string) {
        //  this.value = source;
        if (typeof source === 'string') {
            this.text = source;
            this.value = source;
        }
        if (typeof source === 'object') {
            this.text = textField ? source[textField] : source.text;
            this.value = source[idField];
        }
    }
}