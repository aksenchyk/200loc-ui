import {
    Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation,
    ViewChild, forwardRef, HostBinding, Renderer
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, NgModel } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
    selector: 'loc-input-textArea',
    templateUrl: './input-text-area.html',
    styleUrls: ['./input-text-area.scss'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => InputTextArea),
        multi: true
    }],
    host: {
        '[style.padding-top]': "'15px'"
    }
})

export class InputTextArea implements ControlValueAccessor {
    @Input() name: string;
    @Input() type: string = 'text';
    @Input() label: string;
    @Input() value: any;
    @Input() prefix: string;
    @Input() suffix: string;
    @Input() hasError: string;

    @Input('isDisabled') disabled: boolean = false;
    @Input('always-float-label') alwaysFloatLabel: boolean = true;
    @Input('no-label-float') noLabelFloat: boolean;
    @Input('image') imagable: boolean;
    @Output('focus') focusEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output('blur') blurEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output('change') change: EventEmitter<any> = new EventEmitter<any>();


    ngAfterViewInit() {

    }

    private onTouchedCallback: () => void = () => { };
    writeValue(value: any) {
        if (value != undefined) {
            this.value = value;
        }
    }
    registerOnChange(fn: any) {
        this.change.subscribe(fn)
    }
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
    hasValue() {
        return !!this.value && this.value != 0;
    }
}
