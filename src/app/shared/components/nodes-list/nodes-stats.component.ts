import {
    Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef,
    Output, EventEmitter,
    trigger, state, style, transition, animate, keyframes
} from '@angular/core';
import { AppState, getWorkingNodes } from '../../../core/reducers';
import { Store } from '@ngrx/store';

@Component({
    selector: 'nodes-list',
    templateUrl: './nodes-stats.tmpl.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('iconState', [
            state('up', style({ transform: 'rotate(0deg)' })),
            state('down', style({ transform: 'rotate(180deg)' })),
            transition('up => down', animate('.4s ease-in')),
            transition('down => up', animate('.4s ease-out')),
        ])
    ],
    styles: [
        `
        :host {  
            display: block;
            margin: .625rem;
            width: auto;         
        }
        `
    ]
})
export class NodeListComponent {
    workingNodes: Array<any> = [];
    constructor(
        private _store: Store<AppState>,
        private _cd: ChangeDetectorRef
    ) {

    }
    ngAfterViewInit() {
        this._store.let(getWorkingNodes())
            .subscribe((nodes) => {
                this.workingNodes = [...nodes];
                this._cd.markForCheck();
                console.log('NODES --->', this.workingNodes)
            }, (err) => {
                console.log(err);
            })
    }
}