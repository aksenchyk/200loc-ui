import { Component, OnInit, Input, Output, EventEmitter, HostListener, forwardRef } from '@angular/core';
import {
    NgControl, NgModel,
    ControlValueAccessor, NG_VALUE_ACCESSOR
} from '@angular/forms';

@Component({
    selector: 'loc-switch',
    template: `
    
    <div class="input__switch" [class.on]="value" [class.disabled]='disabled' [ngClass]='type'>
        <div class="switch-container">         
        </div>
    </div>
 
    `,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SwitchComponent),
            multi: true
        }
    ],
    host: {
        //   '[style.display]': "'flex'"
    }
})
export class SwitchComponent implements ControlValueAccessor {
    writeValue(v: any): void {
        if (v !== undefined) {
            this.value = v;
        }

    }
    /**
      * ControlValueAccessor members
      */
    onTouched = () => {
    };

    registerOnChange(fn): void {
        this.change.subscribe(fn);
    }

    setDisabledState(isDisabled: boolean) {
        this.disabled = isDisabled;
    }


    registerOnTouched(fn): void {
        this.onTouched = fn;
    }
    @Input() disabled: boolean;
    @Input() type: string = 'circle';
    @Input() value: boolean;
    @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();


    @HostListener('click', ['$event'])
    private onClick(event): void {
        if (!this.disabled)
            this.toggleValue();
    }

    public toggleValue(): void {
        this.value = !this.value;
        this.change.emit(this.value);
    }
}