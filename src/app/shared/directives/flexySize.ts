import { Directive, ElementRef, Renderer, Input, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';


@Directive({
    selector: '[flexy]',
})
export class FlexySize {

    @Input() bottom: number = 0;

    ignoreElementSize = false;
    constructor(public element: ElementRef, private renderer: Renderer, @Inject(DOCUMENT) private _doc: any) { }

    ngAfterViewInit() {
        this.renderer.listenGlobal('window', 'resize', (evt: any) => {
            this._setMinHeight();
        });
        this._setMinHeight();
    }

    _setMinHeight() {       
        var docHeight = this._doc.documentElement.clientHeight;
        var docWidth = this._doc.documentElement.clientWidth;      
        let rect = this.element.nativeElement.getBoundingClientRect(this.element.nativeElement);
        if (docWidth < 768) {
            this.renderer.setElementStyle(this.element.nativeElement, 'min-height', `${docHeight - rect.top}px`);
        } else {
            this.renderer.setElementStyle(this.element.nativeElement, 'min-height', `${docHeight - rect.top - this.bottom}px`);
        }
    }
    _reset() {
        this.renderer.setElementStyle(this.element.nativeElement, 'min-height', `auto`);
        this.renderer.setElementStyle(this.element.nativeElement, 'transition-duration', `${0}ms`);
    }
}