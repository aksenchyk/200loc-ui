import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
    selector: '[startWith]',
    providers: [{ provide: NG_VALIDATORS, useExisting: StartsWithDirectiveValidator, multi: true }]
})
export class StartsWithDirectiveValidator implements Validator {
    @Input('startWith') expr: string;

    validate(control: AbstractControl) {
        if (control.value && !control.value.startsWith(this.expr)) {
            return { 'startWith': control.value };
        }
        return null;
    }
}