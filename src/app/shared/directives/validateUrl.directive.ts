import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[validateUrl][formControlName],[validateUrl][formControl],[validateUrl][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => UrlValidator), multi: true }
    ]
})
export class UrlValidator implements Validator {
    constructor() { }
    regExp = /^(https?):\/\/([A-Z\d\.-]{2,})(:\d{2,4})?/i
    validate(c: AbstractControl): { [key: string]: any } {
        let v = c.value;
        return (!this.regExp.test(v)) ? {
            validateUrl: true
        } : null;
    }
}
