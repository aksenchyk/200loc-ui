import { Injectable, NgZone, isDevMode } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ReplaySubject, Observable } from "rxjs";

import { Store } from '@ngrx/store';
import { AppState, isUserLoggedIn, getHealth } from '../../core/reducers';
import { DefaultsActions, HealthActions, StatsActions, NodesActions } from '../../core/actions';
import { PluginApi, ServiceApi, LoopBackAuth, StatsService } from '../../core/services';

@Injectable()
export class AppController {
    private _init$: ReplaySubject<any> = new ReplaySubject();  
    constructor(
        private _store: Store<AppState>,
        private _defaultsActions: DefaultsActions,
        private _healthActions: HealthActions,
        private _statsActions: StatsActions,
        private _nodesActions: NodesActions,
        private _pluginsApi: PluginApi,
        private _authService: LoopBackAuth,
        private _servicesApi: ServiceApi,
        private _statsService: StatsService,
        private _ngZone: NgZone) { }

    get init$() {
        // share reference to the Producer
        return this._init$.asObservable().share();
    }

    start() { 
        setInterval(() => {
            this._statsService.stats('info')
                .subscribe((responce) => {
                    this._store.dispatch(this._healthActions.setHealth(responce.health || []));
                    this._store.dispatch(this._nodesActions.setNodes(responce.nodes || []));
                },(err)=>{
                    console.log(err);
                })
        },2000)

        this._store
            .let(isUserLoggedIn())
            .subscribe((isLoggedIn) => {
                if (isLoggedIn == true) {
                    this._store.dispatch(this._defaultsActions.setLoading());
                    this._ngZone.runOutsideAngular(() => {
                        this._loadAppDefaults((defaults) => {
                            this._ngZone.run(() => {
                                this._store.dispatch(this._defaultsActions.setDefaults(defaults));
                                this._init$.next(true);
                            });
                        })
                    });
                }
            });
        this._authService.populate();
    }

    _loadAppDefaults(doneCallback: (defaults: any) => void) {
        Observable.zip(
            this._pluginsApi.find(),
            this._servicesApi.find(),
            (plugins, services, proxy) => [plugins, services, proxy])
            .subscribe(value => {
                doneCallback({
                    plugins: value[0],
                    services: value[1]
                });
            }, err => {
                console.error(err);
            })
    }
}
