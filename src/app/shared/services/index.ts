import { AppController } from './controller.service';

export * from './controller.service';

export var SHARED_SERVICES: Array<any> = [
    AppController
];
