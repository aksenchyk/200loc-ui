import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router'
import { ModalModule, BsDropdownModule, TabsModule } from 'ngx-bootstrap'
import { SHARED_SERVICES } from './services';
import { SHARED_COMPONENTS } from './components';
import { SHARED_DIRECTIVES } from "./directives";
import { APP_PIPES_PIPES } from "./pipes";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DndModule } from 'ng2-dnd';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    ...SHARED_COMPONENTS,
    ...SHARED_DIRECTIVES,
    ...APP_PIPES_PIPES
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    HttpModule,
    ModalModule.forRoot(), /* todo: subject to review */
    BsDropdownModule.forRoot(),/* todo: subject to review */
    TabsModule.forRoot(),/* todo: subject to review */
    NgbModule,
    DndModule.forRoot(),/* todo: subject to review */
    ChartsModule
  ],
  exports: [
    ...SHARED_COMPONENTS,
    ...SHARED_DIRECTIVES,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule,
    BsDropdownModule,
    TabsModule,
    ...APP_PIPES_PIPES,
    NgbModule,
    DndModule,
    ChartsModule
  ],
  providers: [
    ...SHARED_SERVICES
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      // Core singletons
      providers: [
        ...SHARED_SERVICES
      ]
    };
  }

}
